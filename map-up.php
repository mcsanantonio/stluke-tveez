<?php include('header.php');?>

<?php
  require_once "kiosk_config.php";
  $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
  if ($db->connect_error) {
    die('Connect Error (' . $db->connect_errno . ') '
    . $mysqli->connect_error);
  }
  $kiosk_id = isset($_GET['id'])? addslashes($_GET['id']):"";
  $select_query = "SELECT * FROM kiosk02 AS d WHERE id = ".mysqli_real_escape_string($db,$kiosk_id)." LIMIT 1";
  $result = $db->query($select_query);  
?>

<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Location Map</h1>
  <div class="bg_area">
    <div class="main-content-area" style="padding-bottom:10px">
	<div class="row two-thirds">
       <div id='canvas1' class="map1" style="height: 450px;background:#ccc"></div>
	</div>
    <div class="row one-third">
	<div class="walkthrough">
  <?php if($result->num_rows == 1):?>
   <?php $kiosk02 = $result->fetch_object();?>
   
    <h2>How to get to <span><?php if(!empty($kiosk02->destination)) { echo "{$kiosk02->destination}"; } else { echo "N/A"; } ?></span></h2>
	<ul class="walk numb">
	  <?php if(!empty($kiosk02->step1)) { echo "<li> {$kiosk02->step1} </li>"; } ?>
	  <?php if(!empty($kiosk02->step2)) { echo "<li> {$kiosk02->step2} </li>"; } ?>
	  <?php if(!empty($kiosk02->step3)) { echo "<li> {$kiosk02->step3} </li>"; } ?>
	  <?php if(!empty($kiosk02->step4)) { echo "<li> {$kiosk02->step4} </li>"; } ?>
	</ul>
   <?php else:?>
    <div id="result-list">No destination found.</div>
    <?php endif;?>
    </div>
    </div>
      <div id="cont_remember">
        <i class="fa fa-camera"></i> Can't remember? Take a picture.
      </div>
  </div>
    </div>
  </div>
</section>
<?php include('footer.php');?>