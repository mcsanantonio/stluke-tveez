<?php include('header.php');?>
<?php
  require_once "kiosk_config.php";
  $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
  if ($db->connect_error) {
    die('Connect Error (' . $db->connect_errno . ') '
    . $mysqli->connect_error);
  }
  $kiosk_id = isset($_GET['id'])? addslashes($_GET['id']):"";
  $select_query = "SELECT * FROM kiosk2 AS d WHERE id = ".mysqli_real_escape_string($db,$kiosk_id)." LIMIT 1";
  $result = $db->query($select_query);  
  
  
  
?>
<script src="js/raphael-min.js"></script>
<script src="map/map.js"></script>
<script>
  jQuery(window).ready( function()
  {
      var paper = Raphael('canvas1', 735, 450);
      var path = drawpath( paper, "<?php echo $kiosk02->path; ?>", 4000, { fill: 'none', stroke: '#E70D2F', 'stroke-width': 5, 'fill-opacity': 0 }, function()
      {
          <?php
            if ($kiosk02->step2 != NULL) :
          ?>
              $('#canvas1').fadeTo('slow', 0.5, function()
				{
                  var secondPath = "<?php echo $kiosk02->path2; ?>";
                  paper.clear();
                  $(this).css('background-image', 'url(assets/img/3rdfloor.png)'); //change svg background
                  var path2 = drawpath(paper, secondPath, 4000, { fill: 'none', stroke: '#E70D2F', 'stroke-width': 5, 'fill-opacity': 0 }, function() {}); // plot 2nd path
				}).fadeTo('slow', 1);
          <?php
            endif;
          ?>
      });
  });
</script>

<span id="back"><a id="" class="" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Location Map</h1>
  <div class="bg_area">
    <div class="main-content-area" style="padding-bottom:10px">
      <div class="row two-thirds">
        <div class="walkthrough">
          <div id='canvas1' class="map1" style="height: 450px;background:#ccc"></div>
        </div>
      </div>
      <div class="row one-third">
        <div class="walkthrough">
          <?php if($result->num_rows == 1):?>
          <?php $ancillary02 = $result->fetch_object();?>
          <h2>How to get to <span><?php echo "{$ancillary02->building}" ?></span></h2>
          <ul class="walk numb">
          <?php if(!empty($ancillary02->step1)) { echo "<li>{$ancillary02->step1}</li>"; } ?>
          <?php if(!empty($ancillary02->step2)) { echo "<li>{$ancillary02->step2}</li>"; } ?>
          <?php if(!empty($ancillary02->step3)) { echo "<li>{$ancillary02->step3}</li>"; } ?>
          <?php if(!empty($ancillary02->step4)) { echo "<li>{$ancillary02->step4}</li>"; } ?>
          </ul>
          <?php else:?>
          <div id="result-list">No destination found.</div>
          <?php endif;?>
        </div>
      </div>
      <div id="cont_remember">
        <i class="fa fa-camera"></i> Can't remember? Take a picture.
      </div>
    </div>
  </div>
</section>
<?php include('footer.php');?>