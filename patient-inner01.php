<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Admission Procedures</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Before Processing of Admission</h2>
  <p>The Admission Department is the patient's first stop when they enter St. Luke's Medical Center. Before admission, patients will be requested to present the following:</p>
  <ul class="bull">
    <li>Doctor's admission order sheet - which indicates specific requirements for the patient's case. In the absence of the doctor's admission order sheet or a doctor known to them, a walk-in patient may be admitted through the Emergency Room.</li>
    <li>Letter of Authorization (LOA) - for Company or HMO/PPO Sponsored Accounts. Patients without LOA will be automatically considered as private-paying patients and an initial deposit will be required. Those who submit the LOA during their confinement may be re-classified from private to Corporate or HMO/PPO sponsored accounts.</li>
    <li>Employee Health Clinic (EHC) Physician's order - for confinement for St. Luke’s Employees.</li>
  </ul>
  <hr>
  <h2>Patient Information Sheet (PIS)</h2>
  <p>Information requested will be written in the PIS and will be used for data gathering. This can be obtained from the patient either on the day of admission or in advance to avoid inconvenience.</p>
  <p>All patients are given an ID band to be worn around the wrist for the duration of their stay at the medical center.</p>
  <hr>
  <h2>Consent Forms</h2>
  <p>The patient will be asked to sign consent forms for hospital care, exclusive supply of medicines, limitation on outside diagnostic reports, release of information to insurance companies and/or patient's employer, and waiver of responsibility on loss of valuables.</p>
  <hr>
  <h2>Emergency Admissions</h2>
  <p>Patients are referred to the Emergency Room (ER) if they do not have an admission order sheet from their attending physician. In such cases, the ER consultant will ask the patient for the name of his/her physician. The ER consultant may only assign an attending physician to the patient's case if he or she does not have a personal physician of choice.</p>


</div>
</div>
</section>
<?php include('footer.php');?>