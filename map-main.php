<?php include('header.php');?>
<?php
  require_once "kiosk_config.php";
  $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
  if ($db->connect_error) {
    die('Connect Error (' . $db->connect_errno . ') '
    . $mysqli->connect_error);
  }
  $kiosk_id = isset($_GET['id'])? addslashes($_GET['id']):"";
  $select_query = "SELECT * FROM kiosk02 AS d WHERE id = ".mysqli_real_escape_string($db,$kiosk_id)." LIMIT 1";
  $result = $db->query($select_query);
?>

<script src="js/jquery.lazylinepainter-1.5.1.min.js"></script>

<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Location Map</h1>
  <div class="bg_area">
    <div class="main-content-area" style="padding-bottom:10px">
	<div class="row two-thirds">
       <div id='demo' class="floorGF"></div>
	</div>
    <div class="row one-third">
	<div class="walkthrough">
  <?php if($result->num_rows == 1):?>
   <?php $kiosk02 = $result->fetch_object();?>
   
    <h2>How to get to <span><?php if(!empty($kiosk02->destination)) { echo "{$kiosk02->destination}"; } else { echo "N/A"; } ?></span></h2>
	<ul class="walk numb">
	  <?php if(!empty($kiosk02->step1)) { echo "<li> {$kiosk02->step1} </li>"; } ?>
	  <?php if(!empty($kiosk02->step2)) { echo "<li> {$kiosk02->step2} </li>"; } ?>
	  <?php if(!empty($kiosk02->step3)) { echo "<li> {$kiosk02->step3} </li>"; } ?>
	  <?php if(!empty($kiosk02->step4)) { echo "<li> {$kiosk02->step4} </li>"; } ?>
	</ul>
   <?php else:?>
    <div id="result-list">No destination found.</div>
    <?php endif;?>

    <button id="replay" type="button">Restart Map</button>

    </div>
    </div>
  </div>
    </div>
  </div>
</section>

<script>
(function( $ ){
  var svgData = {
    "demo" :{"strokepath" :
      [
        { "path": "M404.3,108.3c0,0,15.8,12.3,44.3,8c0,0,31.7-8.7,30,21 c-1.7,29.7-1,67.8,1.7,73.3c3.7,7.7,10.1,15.2,48.7,13.3c38.3-1.8,65.8-0.1,75,5.7c8.8,5.5,5.4,38.2,4.4,41.2l9,6.8l1.5,26.4h-39.3 l-28.1-21.5l5.3-5.4l20.8,0.1l0.3-20.1l4.5-0.1l0.1,3.8l3.9,0.8l0.4,8.4l11.3,0.8l4.9-3.6l4.3,2.6",
          //"path": "M404.3,108.3c0,0,22.5,14.9,51.2,6.9c0,0,23.7-5,22.9,25.5   S464.2,227.6,509,224c44.8-3.5,74.9,1.8,85.6,2.2c10.7,0.5,17.2,49.7,34.9,53.5s22.3-3,22.3-3l1.5-1.2h32.1v-13.1l-43.2,0v12.9   l10.7,0.1",

          "duration": 1500,
          "strokeColor": '#E91E63',
          "strokeWidth": 7,
          "strokeCap" : 'round',
          "strokeJoin" : "bevel"
        },
      ],
      "dimensions" : { "width": 1020, "height":574 }
    }
  }

var path2_used= false;
var path2 = "<?php echo $kiosk02->path2; ?>";
var path =  "<?php echo (!empty($kiosk02->path))? $kiosk02->path : "M404.3,108.3c0,0,15.8,12.3,44.3,8c0,0,31.7-8.7,30,21 c-1.7,29.7-1,67.8,1.7,73.3c3.7,7.7,10.1,15.2,48.7,13.3c38.3-1.8,65.8-0.1,75,5.7c8.8,5.5,5.4,38.2,4.4,41.2l9,6.8l1.5,26.4h-39.3 l-28.1-21.5l5.3-5.4l20.8,0.1l0.3-20.1l4.5-0.1l0.1,3.8l3.9,0.8l0.4,8.4l11.3,0.8l4.9-3.6l4.3,2.6"; ?>";


svgData.demo.strokepath[0].path = path;


$(document).ready(function(){

$('#replay').click(function(){
    svgData.demo.strokepath[0].path = path;
    $('#demo').removeClass('floor2F floor3F floor5F floor6F floor7F floor4F floor8F floor9F floor10F floor11F floor12F floor13F floor14F floor15F').addClass('floorGF');
    $('#demo').lazylinepainter('erase');
    $('#demo').lazylinepainter('paint');
    path2_used= false;
});

$('#demo').lazylinepainter({
  'svgData' : svgData,
  'strokeWidth': 1,
  'strokeColor':'#dc908e',
  'strokeCap' : 'butt',
  'onComplete' : function(){
    console.log('>> onComplete');
    <?php if($result->num_rows == 1) { 
      if(!empty($kiosk02->path2)){
    ?>
    
    var newClass = "floor<?php echo $kiosk02->floor; ?>";
    if(path2_used==false){ 
        svgData.demo.strokepath[0].path = path2;
        $('#demo').removeClass('floorGF floor2F floor3F floor5F floor6F floor7F floor4F floor8F floor9F floor10F floor11F floor12F floor13F floor14F floor15F').addClass(newClass);
        $('#demo').lazylinepainter('erase');
        $('#demo').lazylinepainter('paint');
    }
    path2_used = true;
    <?php
      }
    }
    ?>

  },
  'onStart' : function(){
  console.log('>> onStart');
  }
  })

var state = 'paint';
  $('#demo').lazylinepainter(state);
  $(window).on('click', function(){
    state = (state === 'erase') ? 'paint':'erase' ;
    $('#demo').lazylinepainter(state);
      console.log('>> ' + state);
  });
})
})( jQuery );
</script>

<?php include('footer.php');?>
