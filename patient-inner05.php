<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Patient Services</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Patient Services</h2>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Ambulance Service</h3>
  <p>For patients who have to be transported to and from St. Luke's Medical Center – Global City, ambulance service within Metro Manila can be made available. Relatives of patients may coordinate with Emergency Care Services at (632) 7897700 ext. 1036.</p>
  <p>Free 24/7 Ambulance Service is also available for select Bonifacio Global City and Makati areas only. Call (632) 846-5455 for Emergency Quick Response.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Corporate Concierge Service</h3>
  <p>St. Luke's provides 24/7 Corporate Concierge Service. Also stationed are 60 Patient Experience Officers at the patient's beck and call. Our friendly and knowledgeable staff will make sure your needs are met.</p>
    </div>
  </div>
    <br style="clear:both">
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Private Duty Nurse</h3>
  <p>Patients needing the services of a Private Duty Nurse (PDN) or a special nurse may inquire from the Nurse's Station or the Nursing Service Office.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Pastoral Care and Education</h3>
  <p>In line with St. Luke's holistic approach to healing, facilities and services are offered to patients and their families to address their spiritual and emotional needs.</p>
  <p>A chapel located at the 5th floor of the Main Hospital Building is open 24 hours a day to provide spiritual refuge to everyone.</p>
  <h3>Daily Mass Schedule:</h3>
  <ul class="no-bull">
    <li><i class="fa fa-calendar"></i> Sundays to Fridays 12:15 PM</li>
    <li><i class="fa fa-calendar"></i> Saturdays 6:00 PM</li>
  </ul>
    </div>
  </div>

</div>
</div>
</section>
<?php include('footer.php');?>