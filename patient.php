<?php
  include('header.php');
  include('functions/search.php');

  $result = search_all("patient_services");
  //print_r($result);
?>

<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Patient Services</h1>
<ul class="btn-set1">
	<?php if($result->num_rows > 0):?>
	  	<?php $counter = 1;?>
	    <?php while($services = $result->fetch_object()):?>
		  <li><a href="map-main.php?id=<?php echo $services->map_id; ?>"><?php echo $services->patient_services_name; ?></a></li>
		<?php endwhile; ?>
  	<?php endif;?>
</ul>
</section>
<?php include('footer.php');?>