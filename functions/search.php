<?php
/**
 * [Search function description]
 * @param  [string] $table  [table name]
 * @param  [string] $filter [search by]
 * @param  [string] $search [description]
 * @return [array]          [search results]
 */

function search($table, $filter, $search, $orderby = NULL)
{
	$db_host = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "tveezph_kiosk";

	@mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
	@mysql_select_db("$db_name") or die("No database");

	$db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
	if ($db->connect_error) {
		die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
	}

	$query = "";	

	if (!empty($search) && $filter == 'id'):
		$query = 'WHERE `'.$filter.'` = "'.addslashes($search).'"';
	elseif(!empty($search)):
		$query = 'WHERE `'.$filter.'` LIKE "%'.addslashes($search).'%"';
	endif;

	if ($orderby != NULL):
		$filter = $filter.",".$orderby;
	endif;

	$search_query = "SELECT * FROM $table AS d $query ORDER by $filter ASC";
	//echo $search_query;
	return $db->query($search_query);

	$db->close();
}

function search_doc_hmo($table, $filter, $search, $orderby = NULL)
{
	$db_host = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "tveezph_kiosk";

	@mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
	@mysql_select_db("$db_name") or die("No database");

	$db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
	if ($db->connect_error) {
		die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
	}

	$search_query = "SELECT * FROM $table JOIN hmo_info ON doctor_hmo.hmo_id = hmo_info.id WHERE doctor_hmo.doctor_id = '$search'";

	return $db->query($search_query);

	$db->close();
}

function search_all($table)
{
	$db_host = "localhost";
	$db_user = "root";
	$db_pass = "";
	$db_name = "tveezph_kiosk";

	@mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
	@mysql_select_db("$db_name") or die("No database");

	$db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
	if ($db->connect_error) {
		die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
	}

	$search_query = "SELECT * FROM $table";

	return $db->query($search_query);

	$db->close();
}
function doctorsList(){

    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "tveezph_kiosk";

    @mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
    @mysql_select_db("$db_name") or die("No database");

    $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
    if ($db->connect_error) {
            die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
    }

    $search_query = "SELECT id, surname, firstname, middle, suffix, specialization, specialty, clinic, local, directline FROM doctor_info ORDER BY surname";
    
    return $db->query($search_query);
}

function deptList(){

    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "tveezph_kiosk";

    @mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
    @mysql_select_db("$db_name") or die("No database");

    $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
    if ($db->connect_error) {
            die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
    }

    $search_query = "SELECT id, center_dept_name, map_id FROM center_dept ORDER BY center_dept_name";
    
    return $db->query($search_query);
}

function procList(){

    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "tveezph_kiosk";

    @mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
    @mysql_select_db("$db_name") or die("No database");

    $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
    if ($db->connect_error) {
            die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
    }

    $search_query = "SELECT id, test_proc_name, map_id FROM test_procedure ORDER BY test_proc_name";
    
    return $db->query($search_query);
}

function searchHMOdoctors($id){

    $db_host = "localhost";
    $db_user = "root";
    $db_pass = "";
    $db_name = "tveezph_kiosk";

    @mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
    @mysql_select_db("$db_name") or die("No database");

    $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
    if ($db->connect_error) {
            die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
    }

    $search_query = "SELECT * FROM doctor_hmo dh 
	                  INNER JOIN doctor_info di ON (dh.doctor_id=di.id)
					  WHERE dh.hmo_id = '{$id}'";
    
    return $db->query($search_query);
}
?>