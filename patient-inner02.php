<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Accommodations</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Accommodations</h2>
  <p>When it comes to accommodations for patients, their families and guests, foremost to St. Luke's are comfort, convenience, and freedom of choice. From the most basic rooms to fully-furnished suites, all patient accommodations are provided with the best available equipment in its class. In most cases, St. Luke's has taken and applied the best practices in the field of hospitality in its premises, providing services and amenities such as a selective menu, private butler service and cable television. St. Luke's believes that comfort and a positive experience are vital in the quest for health and recovery.</p>
  <p>St. Luke's Medical Center – Global City has a range of patient rooms designed to meet practically every conceivable need, thus making them some of the most sought-after choices when confined at the hospital.</p>
  <hr>
  <h2>Room Queuing Procedures</h2>
  <ol class="decimal">
    <li>The Admission Department entertains all room queuing requests whether through e-mail, phone call or personal submission of Doctor's Order by patient/relative, Attending Physician, etc.
	  <ul class="bull">
      <li>The actual date and time of admission of patient will be indicated on the Doctor's Order form.</li>
      <li>The Room Queuing Form must be accomplished and attached by the Admission Associate to the Doctor's Order form. The Room Queuing Form will then be the basis for assignment of patient's room.</li>
	  </ul>
	</li>
    <li>Patients with room/bed queuing for the day are queued based on the date and time that the requests were received by the Admission Associate. The Room Monitoring Coordinator (RMC) shall assign the patient's preferred room, also taking into consideration the patient's case/illness.</li>
    <li>Priorities in room/bed queuing will be given to patients with emergency cases and patients who are scheduled for Cathlab, open heart surgery procedures, and Intensive Care Unit (ICU), Coronary Care Unit (CCU) / Pediatric Intensive Care Unit (PICU) patients.</li>
    <li>It is advisable for non-emergency patients to seek admission between 12:00NN to 5:00PM, when most patients are going home and more rooms are available.</li>
    <li>In cases where no preferred rooms are available, patients/relatives will be offered alternative rooms and will be transferred to the room of choice upon availability.</li>
    <li>The assigned room shall only be held for the patient up to a maximum of two (2) hours beyond the patient's scheduled time of arrival. If the patient fails to arrive within the maximum allowance, the queued room/bed will be assigned to the other waiting patients, if any. The Admission Department will contact the patient and advise him that his room of choice will be released. A new queuing request will need to be secured for another room if the patient decides to continue with his admission later on.</li>
  </ol>
  <hr>
  <h2>Patients' Rooms</h2>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>4-Bed Ward Rooms</h3>
  <small>Essentials and Quality Care</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>The Ward Rooms Section can accommodate four patients in an air-conditioned room. With a total of 71.13 sqm., each section has a bedside table and chair and share one common toilet and bath. Patients are assured of the strictest standards in hygiene and cleanliness undertaken by hospital’s staff. It is equipped with a side cabinet, electronic vault and chair for the patient’s relative.</p>
    </div>
  </div>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>2-Bed Private Room (Semi-Private)</h3>
  <small>Comfortable and Economical</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>The comfortable 2-Bed Semi-Private Room can accommodate two patients in a 37.60 sqm. space. It is equipped with basic amenities including a telephone, over-bed table, cabinet and guest chair for each section, with a shared toilet and bath. Safety deposit vaults are also provided for each patient for security of belongings.</p>
    </div>
  </div>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>Small/Regular Private Room</h3>
  <small>Privacy, Comfort and Affordability</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>Ideal for patients who desire privacy and comfort at very reasonable cost. With a total area of 23 to 25 sqm., the room is provided with adequate amenities to make the patient's stay at the hospital comfortable and conducive for fast recovery.</p>
    </div>
  </div>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>De Luxe Private Room</h3>
  <small>Providing Medical Care in Modern-Day Comfort</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>More spacious than the regular private room, it is designed to provide medical care in modern-day comfort. With a total area of 37.60 sqm., it comes with a 32-inch Wall Mounted Plasma TV, DVD Player, Refrigerator, Recliner & Day Bed.</p>
    </div>
  </div>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>Executive Private Room</h3>
  <small>More Space, Greater Comfort</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>Slightly bigger than the De Luxe Private Room, it has a total area of 46.09 sqm., perfect for patients who want added amenities with a dash of elegance.</p>
  <p>The room is equipped with a 32-inch Wall Mounted Plasma TV, DVD Player, Bose Sound System, Recliner, Refrigerator & Day Bed.</p>
    </div>
  </div>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>Birthing Room</h3>
  <small>Welcome Birth in Safety, Comfort, and Total Privacy</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>Designed to address the distinct needs of expectant parents, this 22.55 sqm. room features a patient's room separated from the living room area with a sleek Japanese style divider. It is equipped with a built-in cardiac monitor, fetal monitor, piped-in oxygen with suction, photo-lighted crib (infant warmer), mini-operating light, anesthesia machine, and Diamond Care DISS nitrous oxide to enable smooth and efficient delivery. Here's a peek into some of the features and amenities inside the birthing room:</p>
    </div>
  </div>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>Acute Stroke Unit (ASU)</h3>
  <small>text information here</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>With a total area of 101.46 sqm. this Unit is designed for patients who have had acute strokes and need seclusion and constant monitoring while recuperating. Within the room are all the equipment needed for highly skilled medical staff for attending to stroke patients.</p>
    </div>
  </div>
  <div class="one-fourth row">
    <div class="the_inner">
  <h3>Isolation Room</h3>
  <small>Total Privacy and Space for Complete Recovery</small>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>The Isolation Room is ideal for patients who require seclusion and ample space during their stay at the hospital. With a total area of 45.59 sqm., the room provides amenities designed to give convenience and comfort to staying relatives as well as visitors.</p>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner">
  <h3>Regular Suite</h3>
  <div class="star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>With a total area of 47 sqm., this room provides all the luxury amenities found in the Executive Suite but in a more compact space.</p>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner">
  <h3>De Luxe Suite</h3>
  <div class="star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></div>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>This room suits patients who want the suite ambience at a slightly higher level of comfort. With a total area of 63.85 sqm., it is more spacious than the Regular Suite, and features all the luxury amenities and casual elegance to make a patient's stay comfortable and conducive to faster recuperation.</p>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner">
  <h3>Executive Suite</h3>
  <div class="star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>The Executive Suit is perfect for patients who want their room to have the ambience of a hotel suite. With a total area of 78.60 sqm., the Executive Suite exudes casual elegance and is complete with luxury amenities for utmost comfort.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner">
    <h3>Ambassador Suite</h3>
  <small>Recuperate and Rest in Total Comfort</small>
  <div class="star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></div>
	<img style="width:49%;display:inline-block" class="responsive" src="images/hotel-deluxe-private.jpg">
	<img style="width:49%;display:inline-block" class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>Designed with total comfort in mind for the patient as well as family and guests, the Ambassador Suite is unsurpassed in its level of services and amenities among Philippine hospital accommodations. Located in one of the most private sections of the hospital building, this impressive Suite features a separate room for the patient, a spacious living and dining area, and complete kitchen facilities. Elegantly-furnished, the patient's room delivers amenities that pamper and satisfy. With a total area of 110.60 sqm., the Suite is separated from the living room with a Varifold divider, providing enough privacy for the delivery of various procedures. It is most conducive to rest, even if family and guests opt to stay with the patient. Here's an inside look at some of the facilities and amenities found inside the Ambassador Suite:</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner">
  <h3>Presidential Suite</h3>
  <small>Showcase of Uncompromising Quality in Patient Accommodation</small>
  <div class="star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
	<img style="width:49%;display:inline-block" class="responsive" src="images/hotel-deluxe-private.jpg">
	<img style="width:49%;display:inline-block" class="responsive" src="images/hotel-deluxe-private.jpg">
  <p>Designed for the exclusive use of the most discriminating of clients, this Suite is tucked away in one of the most secluded sections of the hospital building. With a total area of 157.44 sqm., the Presidential Suite features beautiful interiors to match the highest quality of hospital equipment installed in the room. Its amenities are comparable to those found in presidential suites of luxury hotels.</p>
    </div>
  </div>
</div>
</div>
</section>
<?php include('footer.php');?>