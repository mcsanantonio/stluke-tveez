<?php 

include('header.php');
include('./functions/search.php');
$result = procList();
$procedures = array();
while($proc_list = $result->fetch_assoc()){
   array_push($procedures, $proc_list);
}

?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Tests &amp; Procedures</h1>
      <input type="text" id="procedure_search" data-type='test_proc_name' value=""  />
         <div class="auto-suggest" id="auto-suggest"></div>
      </div>
      <div id="keyboardcont" onmousedown='return false;'>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">q</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">w</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">e</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">r</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">t</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">y</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">u</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">i</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">o</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">p</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="width: 175px;"><i class="fa fa-arrow-left"></i></button></span>
  
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="margin:0 10px 0 52px">a</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">s</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">d</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">f</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">g</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">h</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">j</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">k</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">l</button></span>
        
        <form name="frmbyname" action="test_list.php" id="search_procedure" method="GET">
          <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="width: 240px;" name="search">Search</button></span>
        </form>
        
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="margin:0 10px 0 102px">z</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">x</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">c</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">v</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">b</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">n</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">m</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">.</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">-</button></span>
           
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="width: 1280px;">Space</button></span>
    </div>  
</section>
<script type="text/javascript">

    var procedure_list = <?php echo (count($procedures) > 0)? json_encode($procedures) : false; ?>;

    $(function(){
        /*Search Procedure*/
        $("#search_procedure").on("submit",function(){
            var search_val = $("#procedure_search").val();
            var input = $("<input>").attr({"type"  : "hidden" , "name" : "search"}).val(search_val);
            $(this).append($(input));
            return true;
        });

        /*Keyboard Functionalities*/
        $(".action-button").on("click",function(){
            var container = $("#procedure_search");
            var current_value = container.val();
            switch(($(this).text()).toLowerCase()){
                case "space" : 
                    current_value += " ";
                break;
                case "search" : 
                    current_value += "";
                break;
                case "back" : 
                    current_value += "";
                break;
                case "help" : 
                    current_value += "";
                break;
                case "":
                    var icon = $(this).children('i');
                    if(icon.hasClass("fa-arrow-left")){
                        var len = current_value.length;
                        current_value  = current_value.substr(0,len-1);
                    }
                break;
                default : 
                current_value += $(this).text();
            }
            container.val(current_value);
            $('#procedure_search').trigger('change'); 
        });
    });
</script>
<?php include('footer.php');?>