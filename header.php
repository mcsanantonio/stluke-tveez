<!DOCTYPE html>
<html class="no-js" lang="en"oncontextmenu='return false;'>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>St. Luke's</title>
  <style>
    @import url('assets/css/swiper.min.css');
    @import url('assets/css/font-awesome.min.css');
    @import url('assets/css/jquery.bullseye.css');
  </style>
  <link rel="stylesheet" href="style.css">
  <script src="assets/js/jquery-1.11.2.min.js"></script>
  <script src="assets/js/jquery.idle.js"></script>
</head>
