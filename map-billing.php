<?php include('header.php');?>
<script src="js/jquery.lazylinepainter-1.5.1.min.js"></script>
<script>
(function( $ ){
  var svgData = {
    "demo" :{"strokepath" :
      [
        { 
          
          "path": "M405.5,110.5c0,0,8.3,10.8,49,3.8c0,0,26-7.3,24.3,12.3 s-4.3,62.8-3.8,72.5s9.3,26,47.3,25c0,0,26.5-3,27,8.1H578v23.8h-2.6l-14.3,9.4l-15.4,13.1l-35-27.6l16.1-12.9l0.3-5.8l21.3,0.1",
          //"path": "M405.8,109.3c0,0,4.1,11.4,17,10.6 s26.1,0.4,26.5-6.9s4.3-25.5-8.8-25.8s-20,0-20,0L419,86V74.5l-6.3-1.8l0.8-12c0,0-22.3-1.8-38.3-16.5h-25.8l-7,45.6l75.6,1.2 l0.8-4.6",

          "duration": 1500,
          "strokeColor": '#E91E63',
          "strokeWidth": 3
        },
      ],
      "dimensions" : { "width": 1020, "height":574 }
    }
  }
$(document).ready(function(){
$('#demo').lazylinepainter({
  'svgData' : svgData,
  'strokeWidth':7,
  'strokeColor':'#dc908e',
  'onComplete' : function(){
    console.log('>> onComplete');
  },
  'onStart' : function(){
  console.log('>> onStart');
  }
  })

var state = 'paint';
  $('#demo').lazylinepainter(state);
  $(window).on('click', function(){
    state = (state === 'erase') ? 'paint':'erase' ;
    $('#demo').lazylinepainter(state);
      console.log('>> ' + state);
  });
})
})( jQuery );
</script>

<?php
  require_once "kiosk_config.php";
  $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
  if ($db->connect_error) {
    die('Connect Error (' . $db->connect_errno . ') '
    . $mysqli->connect_error);
  }
  $kiosk_id = isset($_GET['id'])? addslashes($_GET['id']):"";
  $select_query = "SELECT * FROM kiosk02 AS d WHERE id = ".mysqli_real_escape_string($db,$kiosk_id)." LIMIT 1";
  $result = $db->query($select_query);  
?>

<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Location Map</h1>
  <div class="bg_area">
    <div class="main-content-area" style="padding-bottom:10px">
	<div class="row two-thirds">
       <div id='demo'></div>
	</div>
    <div class="row one-third">
	<div class="walkthrough">
  <?php if($result->num_rows == 1):?>
   <?php $kiosk02 = $result->fetch_object();?>
   
    <h2>How to get to <span><?php if(!empty($kiosk02->destination)) { echo "{$kiosk02->destination}"; } else { echo "N/A"; } ?></span></h2>
	<ul class="walk numb">
	  <?php if(!empty($kiosk02->step1)) { echo "<li> {$kiosk02->step1} </li>"; } ?>
	  <?php if(!empty($kiosk02->step2)) { echo "<li> {$kiosk02->step2} </li>"; } ?>
	  <?php if(!empty($kiosk02->step3)) { echo "<li> {$kiosk02->step3} </li>"; } ?>
	  <?php if(!empty($kiosk02->step4)) { echo "<li> {$kiosk02->step4} </li>"; } ?>
	</ul>
   <?php else:?>
    <div id="result-list">No destination found.</div>
    <?php endif;?>
    </div>
    </div>
  </div>
    </div>
  </div>
</section>
<?php include('footer.php');?>