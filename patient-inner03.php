<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Health Maintenance Organization (HMO)</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>The HMO Concierge: The First and Only in the Country</h2>
  <p>HMO members are provided with premium-class medical services and unparalleled standards of customer care at St. Luke’s Medical Center-Global City. We strive to provide you comfort and satisfaction every time you access medical services as we take care of important details on your behalf.</p>
  <p>At the HMO Concierge, you get the assistance you truly deserve from our HMO Specialists because we value your time and convenience.</p>
  <hr>
  <h2>Serving the Needs of HMO Members</h2>
  <p>As our esteemed HMO client, we are committed to assist you with your healthcare needs.</p>
  <p>We will refer you to and arrange an appointment with an HMO-accredited doctor or specialist. We can also schedule your laboratory and other diagnostic procedures.</p>
  <p>Our HMO Specialists will coordinate with your HMO for the timely release of your Letter of Authorization (LOA).</p>
  <p>We can also arrange your Annual Executive Check-Up with the St. Luke’s Health and Wellness Center.</p>
  <p>For Philhealth members, we will guide you in completing your documentary requirements for the processing of your benefits.</p>
  <hr>
  <h2>Providing Medical Planners to Help You</h2>
  <p>Hospitalization can cause a lot of stress and worries, particularly costs and the process of hospital admission. Because we aim for you to have peace of mind, we have appointed Medical Planners who will provide you with medical and financial planning assistance.</p>
  <p>What can you expect from our Medical Planners? They will assess your medical requirements and offer you several options whether this be related to choice of rooms and other medical services. This is to help you conserve your Maximum Benefit Limit. We aim to give you a variety of options that would minimize our utilization expenses thus eliminating unnecessary expenditures. We believe hospitalization need not be beyond your budget. For inquiries, visit us at the HMO Concierge, Monday to Saturday from 8:00 A.M. to 6:00 P.M.</p>
  <h3>Accredited HMOS at St. Luke’s – Global City</h3>
  <ul class="bull">
    <li>IntelliCare (Asalus Corporation)</li>
    <li>Blue Cross Healthcare, Inc.</li>
    <li>Cocolife Health Care</li>
    <li>EastWest Healthcare, Inc.</li>
    <li>Fortune Medicare, Inc.</li>
    <li>Health Maintenance, Inc.</li>
    <li>Health Plan Philippines, Inc.</li>
    <li>Insular Health Care, Inc.</li>
    <li>Maxicare HealthCare Corporation</li>
    <li>MEDICard Philippines, Inc.</li>
    <li>Medicare Plus, Inc.</li>
    <li>MEDOCare Health Systems, Inc.</li>
    <li>PhilCare (Philhealth Care Inc.)</li>
    <li>ValuCare Health Systems, Inc.</li>
  </ul>
  <h3>Other Accounts with HMO Products Assisted at the HMO Concierge</h3>
  <ul class="bull">
    <li>AsianLife and General Assurance Corporation</li>
    <li>Generali Philippines</li>
  </ul>
  <hr>
  <h2>Health Maintenance Organizations (HMOs)</h2>
  <p>The choice of doctor for each HMO is based on the list of accredited doctors provided by the respective HMOs. Please refer to our Doctor’s Directory to find an HMO-accredited doctor.
  <p>If you are a member of a Health Maintenance Organization, check above whether your HMO is accredited by St. Luke's Medical Center. Before admission, make sure to coordinate with your HMO so that all pre-authorization documents can be prepared in advance to prevent unnecessary delays and inconvenience at the hospital's Admission office.
  <h3>Preferred Providers Organizations (PPOs)</h3>
  <ul class="bull">
    <li>Bupa International</li>
    <li>Calvo's Select Care(formulary)</li>
    <li>JMI ASIA-PACIFIC, INC.</li>
    <li>NetCare Life & Health Insurance Company(formulary)</li>
    <li>Staywell Insurance(formulary)</li>
    <li>Takecare Insurance Co.(formulary)</li>
    <li>Vanbreda International</li>
    <li>Aetna International</li>
    <li>Seven Corners, Inc.</li>
  </ul>
</div>
</div>
</section>
<?php include('footer.php');?>