<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Concierge</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Concierge</h2>
  <img style="width:45%;display:block;float:left;margin:0 25px 0 0" class="responsive" src="images/concierge.png">
  <p>Corporate Concierge offers personalized, executive-level assistance tailored-fit to your needs. We accommodate requests - from regular to extraordinary - as long as these are ethical.  </p>
  <p>Our Officers delightfully answer your questions and take care of your needs with quality service usually found only in the world's finest hotels. </p>
  <p>These services can range from airport transfers for those who are flying domestic or international, visa extension due to the medical condition of foreign patients, airline bookings, ordering of flowers, heliport usage or even just a simple request of borrowing from our in-house collection of DVDs and CDs available to make your stay more pleasant.</p>


</div>
</div>
</section>
<?php include('footer.php');?>