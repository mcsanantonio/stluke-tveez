<?php
  include('header.php');
  include('functions/search.php');

  $search = isset($_GET['id'])? addslashes($_GET['id']) : "";
  $search = trim($_GET['id']);

  $result = search("hmo_info","id",$search);
?>

<span id="back"><a id="" class="" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Health Maintenance Organization (HMO)</h1>
  <div class="main_bg">
    <div class="row one-third">
      <?php if($result->num_rows == 1):?>
      <?php $hmo_info = $result->fetch_object();?>
      <ul class="doc-info the_hmo_list">
        <li class="big-text" id="hmo_title"><?php echo "{$hmo_info->hmo_name}" ?></li>
        <?php if(!empty($hmo_info->coordinator)){ echo "
        <li>Plan Coordinator</li>
        <li class='big-text'>{$hmo_info->coordinator}</li>
        ";}
        ?> 
        <?php if(!empty($hmo_info->directline)){ echo "
        <li>Secretary/Contact numbers</li>
        <li class='big-text'>{$hmo_info->directline}</li>
        ";}
        ?> 
        <?php if(!empty($hmo_info->local)){ echo "
        <li><span>Trunkline</span>Local</li>
        <li class='big-text'><span>(02)789-7700</span>{$hmo_info->local}</li>
        ";}
        ?> 
        <?php if(!empty($hmo_info->dayshours)){ echo "
        <li>Operating Days and Hours</li>
        <li class='big-text'>{$hmo_info->dayshours}</li>
        ";}
        ?> 
        <?php if(!empty($hmo_info->room)){ echo "
        <li>Location</li>
        <li class='big-text'>{$hmo_info->room}</li>
        ";}
        ?>
      </ul>
      <div class="doc-info-map">
        <a class="directions" href="map-main.php?id=<?php echo $hmo_info->map_id?>">Click to get directions</a>
      </div>
      <?php else:?>
      <div id="result-list">No doctor record found.</div>
      <?php endif;?>
    </div>
<?php
   $result_doc = searchHMOdoctors($search);
   $result_cnt = ($result_doc) ? $result_doc->num_rows : "No records found.";
   //if ($result_doc):
?>
    <div class="row two-thirds">
      <div id="records" style="color:#000">Number of records found : <span style="color:#000"><?php echo $result_cnt; ?></span></div>
      <ul class="doclist" id="doclist_title" style="padding:15px 0">
        <li>&nbsp;</li>
        <li>Specialization</li>
        <li>Doctor's Name</li>
      </ul>
      <div class="bg_area" id="all_list" style="height:460px">
        <?php if((isset($result_doc->num_rows)) > 0) :?>
        <?php $counter = 1;?>
        <?php while($doctor_info = $result_doc->fetch_object()):?>
        <a href="doctor_info.php?id=<?php echo $doctor_info->id?>" style="padding: 0; <?php echo $counter++%2 == 1 ? 'background: #FFFFFF;' : 'background: #E3F2FD;' ?>">
          <ul class="doclist" style="padding:5px 0">
            <li class="view">View Info</li>
            <li><?php echo "{$doctor_info->specialization}" ?></li>
            <li><?php echo "{$doctor_info->surname}, {$doctor_info->firstname} {$doctor_info->middle}"?><?php if(!empty($doctor_info->suffix)) { echo ", {$doctor_info->suffix}"; } ?></li>
          </ul>
        </a>
        <?php endwhile;?>
        <?php else: ?>
        No record found.
        <?php endif;?>
        
      </div>
    </div>
<?php
	//endif;
?>
  </div>
</section>
<?php include('footer.php');?>
