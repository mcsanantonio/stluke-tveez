<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Visiting Hours</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Visiting Hours</h2>
  <p>Visitors are requested to pass the Main Lobby entrance from 6:00 AM to 10:00 PM. Only two (2) visitors per patient will be allowed to enter the medical center beyond 10:00 PM. Handwashing before entering a patient's room is highly advised for all guests and visitors.</p>
  <div class="one-third row">
    <div class="the_inner no-minheight">
    <h3>Coronary Care Unit</h3>
	<ul class="no-bull">
      <li><i class="fa fa-clock-o"></i> 10:00-11:00AM</li>
      <li><i class="fa fa-clock-o"></i> 6:00-8:00PM</li>
	</ul>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner no-minheight">
    <h3>Medical Intensive Care Unit</h3>
	<ul class="no-bull">
      <li><i class="fa fa-clock-o"></i> 10:00-11:00AM</li>
      <li><i class="fa fa-clock-o"></i> 6:00-8:00PM</li>
	</ul>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner no-minheight">
    <h3>Neuro-Critical Care Unit</h3>
	<ul class="no-bull">
      <li><i class="fa fa-clock-o"></i> 10:00-11:00AM</li>
      <li><i class="fa fa-clock-o"></i> 3:00-4:00PM</li>
      <li><i class="fa fa-clock-o"></i> 7:00-8:00PM</li>
	</ul>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner no-minheight">
    <h3>Nursery Viewing/Pediatric Intensive Care Unit</h3>
	<ul class="no-bull">
      <li><i class="fa fa-clock-o"></i> 10:00-11:00AM</li>
      <li><i class="fa fa-clock-o"></i> 5:00-6:00PM</li>
	</ul>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner no-minheight">
    <h3>Surgical Intensive Care Unit</h3>
	<ul class="no-bull">
      <li><i class="fa fa-clock-o"></i> 10:00-11:00AM</li>
      <li><i class="fa fa-clock-o"></i> 3:00-4:00PM</li>
      <li><i class="fa fa-clock-o"></i> 7:00-8:00PM</li>
	</ul>
    </div>
  </div>
</div>
</div>
</section>
<?php include('footer.php');?>