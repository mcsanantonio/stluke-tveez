<?php include('header.php');?>
<?php
  require_once "kiosk_config.php";
  $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
  if ($db->connect_error) {
    die('Connect Error (' . $db->connect_errno . ') '
    . $mysqli->connect_error);
  }
  $kiosk_id = isset($_GET['id'])? addslashes($_GET['id']):"";
  $select_query = "SELECT * FROM hmo_info AS d WHERE id = ".mysqli_real_escape_string($db,$kiosk_id)." LIMIT 1";
  $result = $db->query($select_query);  
?>
<span id="back"><a id="" class="" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Location Map</h1>
  <div class="bg_area">
    <div class="main-content-area" style="padding-bottom:10px">
      <div class="row one-third">
        <div class="walkthrough">
          <h2>MAB - Information</h2>
          <img class="img-size" src="images/mab.jpg" alt="">
        </div>
      </div>
      <div class="row two-thirds">
        <div class="walkthrough">
          <?php if($result->num_rows == 1):?>
          <?php $hmo_info = $result->fetch_object();?>
          <h2>How to get to <span><?php echo "{$hmo_info->hmo_name}" ?></span></h2>
          <ul class="walk numb">
          <?php if(!empty($hmo_info->step1)) { echo "<li>{$hmo_info->step1}</li>"; } ?>
          <?php if(!empty($hmo_info->step2)) { echo "<li>{$hmo_info->step2}</li>"; } ?>
          <?php if(!empty($hmo_info->step3)) { echo "<li>{$hmo_info->step3}</li>"; } ?>
          <?php if(!empty($hmo_info->step4)) { echo "<li>{$hmo_info->step4}</li>"; } ?>
          </ul>
          <?php else:?>
          <div id="result-list">No destination found.</div>
          <?php endif;?>
        </div>
      </div>
      <div id="cont_remember">
        <i class="fa fa-camera"></i> Can't remember? Take a picture.
      </div>
    </div>
  </div>
</section>
<?php include('footer.php');?>