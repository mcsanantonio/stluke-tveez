<?php
  include('header.php');
  include('functions/search.php');

  $search = isset($_GET['id'])? addslashes($_GET['id']) : "";
  $search = trim($_GET['id']);

  $result = search("doctor_info","id",$search);
  $hmo_result = search_doc_hmo("doctor_hmo","doctor_id",$search);
?>

<body id="the_frame" class="link1">
<header>
  <a id="logo" href="home.php">St. Lukes</a>
</header>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<?php include('nav.php');?>

<section>
  <?php if($result->num_rows == 1):?>
  <?php $doctor_info = $result->fetch_object();?>
  <h1>Doctor's Information</h1>
  <div class="bg_area">
    <div class="main-content-area">
      <div class="row one-third">
        <ul class="doc-info">
          <li class="big-text">General Information</li>
          <?php if(!empty($doctor_info->surname)) { echo "
          <li>Name of Doctor</li>
          <li class='big-text'>{$doctor_info->surname}, {$doctor_info->firstname} {$doctor_info->middle}";}?><?php if(!empty($doctor_info->suffix)) { echo ", {$doctor_info->suffix}</li>"; } else { echo "</li>
          ";} ?>
          <?php if(!empty($doctor_info->specialization)) { echo "
          <li>Specialization</li>
          <li class='big-text'>{$doctor_info->specialization}</li>
          ";} ?>
          <?php if(!empty($doctor_info->specialty)) { echo "
          <li>Sub-Specialization</li>
          <li class='big-text'>{$doctor_info->specialty}</li>
          ";} ?>
          <?php if(!empty($doctor_info->local)) { echo "
          <li><span>Trunkline</span>Local</li>
          <li class='big-text'><span>(02)789-7700</span>{$doctor_info->local}</li>
          ";} ?>
          <?php if(!empty($doctor_info->directline)) { echo "
          <li>Secretary/Contact numbers</li>
          <li class='big-text'>{$doctor_info->directline}</li>
          ";} ?>
          <?php if(!empty($doctor_info->clinic)) { echo "
          <li>Clinic</li>
          <li class='big-text'>{$doctor_info->clinic}</li>
          ";} ?>
        </ul>
        <div class="doc-info-map">
          <a class="directions" href="map.php?id=<?php echo $doctor_info->map_id?>">Click to get directions</a>
        </div>
      </div>
      <div class="row one-third">
        <ul class="doc-info">
          <li class="big-text">Clinic Schedules</li>
          <?php
            if (isset($doctor_info->mo_am) && isset($doctor_info->mo_pm)):
          ?>
          <li><span>Day</span> Time</li>
          <li class="big-text">
            <span>Monday</span>
            <?php echo "{$doctor_info->mo_am}"; ?>
            <?php echo "{$doctor_info->mo_pm}"; ?>
          </li>
          <?php 
            endif;
          ?>

          <?php
            if (isset($doctor_info->tu_am) && isset($doctor_info->tu_pm)):
          ?>
          <li><span>Day</span> Time</li>
          <li class="big-text">
            <span>Tuesday</span>
            <?php echo "{$doctor_info->tu_am}"; ?>
            <?php echo "{$doctor_info->tu_pm}"; ?>
          </li>
          <?php 
            endif;
          ?>

          <?php
            if (isset($doctor_info->we_am) && isset($doctor_info->we_pm)):
          ?>
          <li><span>Day</span> Time</li>
          <li class="big-text">
            <span>Wednesday</span>
            <?php echo "{$doctor_info->we_am}"; ?>
            <?php echo "{$doctor_info->we_pm}"; ?>
          </li>
          <?php 
            endif;
          ?>

          <?php
            if (isset($doctor_info->th_am) && isset($doctor_info->th_pm)):
          ?>
          <li><span>Day</span> Time</li>
          <li class="big-text">
            <span>Thursday</span>
            <?php echo "{$doctor_info->th_am}"; ?>
            <?php echo "{$doctor_info->th_pm}"; ?>
          </li>
          <?php 
            endif;
          ?>

          <?php
            if (isset($doctor_info->fr_am) && isset($doctor_info->fr_pm)):
          ?>
          <li><span>Day</span> Time</li>
          <li class="big-text">
            <span>Friday</span>
            <?php echo "{$doctor_info->fr_am}"; ?>
            <?php echo "{$doctor_info->fr_pm}"; ?>
          </li>
          <?php 
            endif;
          ?>

          <?php
            if (isset($doctor_info->sa_am) && isset($doctor_info->sa_pm)):
          ?>
          <li><span>Day</span> Time</li>
          <li class="big-text">
            <span>Saturday</span>
            <?php echo "{$doctor_info->sa_am}"; ?>
            <?php echo "{$doctor_info->sa_pm}"; ?>
          </li>
          <?php 
            endif;
          ?>

          <?php if(!empty($doctor_info->appointment)) { echo "
          <li><span>Note:</span></li>
          <li class='big-text'>{$doctor_info->appointment}</li>
          ";} ?>
      	</ul>
    </div>
    <div class="row one-third">
     	<ul class="doc-info-accredited">
			<?php
			if (count($hmo_result) > 0):
				echo '<li class="big-text" style="width:100%;font:300 18px/22px Arial, Helvetica, sans-serif !important">HMO</li><br />';

			    while ($obj =  $hmo_result->fetch_object()) {
				    echo '<li class="big-text" style="line-height:29px">'.$obj->hmo_name.'</li>';
				}
			endif;		
		?>
      </ul>
     	<ul class="doc-info-accredited">
          <?php if(!empty($doctor_info->hmo_note)) { echo "
          <li class='big-text' style='width:100%;line-height:29px'>{$doctor_info->hmo_note}</li>
          ";} ?>
      </ul>
  </div>
  <?php else:?>
  <div id="result-list">No doctor record found.</div>
  <?php endif;?>
  </div>
  </div>
</section>
<?php include('footer.php');?>
