<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Discharge Procedures</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Discharge Procedures</h2>
  <p>The cut-off time for discharge is 11:00 AM. An additional half (1/2) day rate will be charged for room and board after 11:00 AM and one (1) day will be charged for room and board after 5:00 PM.</p>
  <p>A written discharge order must be secured from your attending physician. You must secure discharge orders from ALL doctors consulted during your hospitalization.</p>
  <p>Once discharge orders are secured, your nurse on duty will issue a Discharge Notice Slip with an initial print-out of your itemized Statement of Account. Please check the items in your bill before paying at the Cashier. Kindly inform your nurse on duty about any concerns you may have regarding your bill.</p>
  <p>When you are ready to pay your bill, you may then proceed to the Main Cashier. Suite room patients may request for a cashier to collect payment in their room. Once your account is settled, the cashier will give your Official Statement of Account, Official Receipt, and Discharge Clearance.</p>
  <p>Upon submission of the Discharge Clearance to your nurse on duty, you will be issued a Gate Pass after you have physically vacated your room.</p>
  <p>A nursing aide will escort you as you leave the hospital. The Gate Pass must be surrendered to the guard at the main lobby for clearance of your personal belongings.</p>
  <p>Please secure your Parking Validation Ticket from the Information and Concierge or Admission office.</p>


</div>
</div>
</section>
<?php include('footer.php');?>