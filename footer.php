<script src="script.js"></script>
<script src="assets/js/jquery.bullseye.js"></script>
<script src="js/jquery.lazylinepainter-1.5.1.min.js"></script>
<script>
function loadGF(id) {
        $('.bsDemo div.jqBullseye').remove();
}
$('div.symbols_legent input[type=radio]').on('click', function(){

    var id = $(this).attr('id');

    // If class = 5, load gf, lineart then bullseye. Else, go directly to bullseye
    if ($('#'+id).hasClass('5')) {
        console.log('wee');
        $('#demow').show();

        loadGF(id);
    }
    else {
        console.log('woo');

        plotCoordinates(id);
    }

    function loadGF(id) {

          var svgData = {
            "demow" :{"strokepath" :
              [
                { "path": "M404.3,108.3c0,0,15.8,12.3,44.3,8c0,0,31.7-8.7,30,21 c-1.7,29.7-1,67.8,1.7,73.3c3.7,7.7,10.1,15.2,48.7,13.3c38.3-1.8,65.8-0.1,75,5.7c8.8,5.5,5.4,38.2,4.4,41.2l9,6.8l1.5,26.4h-39.3 l-28.1-21.5l5.3-5.4l20.8,0.1l0.3-20.1l4.5-0.1l0.1,3.8l3.9,0.8l0.4,8.4l11.3,0.8l4.9-3.6l4.3,2.6",
                  //"path": "M404.3,108.3c0,0,22.5,14.9,51.2,6.9c0,0,23.7-5,22.9,25.5   S464.2,227.6,509,224c44.8-3.5,74.9,1.8,85.6,2.2c10.7,0.5,17.2,49.7,34.9,53.5s22.3-3,22.3-3l1.5-1.2h32.1v-13.1l-43.2,0v12.9   l10.7,0.1",

          "duration": 5000,
          "strokeColor": '#E70D2F',
          "strokeWidth": 5,
          "strokeCap" : 'round',
                },
              ],
              "dimensions" : { "width": 1020, "height":574 }
            }
          }

        var path =  "<?php echo (!empty($kiosk02->path))? $kiosk02->path : "M685.25,293.5c0,0-11.25,0.25-34.25,0S633.5,256,633.5,256"; ?>";

        svgData.demow.strokepath[0].path = path;


        $('#demow').lazylinepainter({
          'svgData' : svgData,
          'strokeWidth': 1,
          'strokeColor':'#dc908e',
          'strokeCap' : 'butt',
          'onComplete' : function(){
            setTimeout(function(){
                plotCoordinates(id);
                $('#demow').hide();
                $('#demow').lazylinepainter('erase');
            }, 3000);
          },
          'onStart' : function(){
            console.log('>> onStart');
            $('#the_leg').removeClass('floor5F');
          }
          })

        var state = 'paint';
        $('#demow').lazylinepainter(state);

    }

    function plotCoordinates(id) {
        headingtxt = "",
        contenttxt = "";

    switch (id) {
        case "radio1":
            var bsFloors = ['floor5F'],
                floorLabels = ['5th Floor'];
            window.floor5F_toppx = [240];
            window.floor5F_rightpx = [486];
            var headingtxt = "Auditorium (Henry Sy, Sr.)";
            break;
        case "radio2":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor'];
            window.floorGF_toppx = [110, 87, 215];
            window.floorGF_rightpx = [544, 465, 586];
            var headingtxt = "Banks/ATM";
            break;
        case "radio3":
            var bsFloors = ['floor5F'],
                floorLabels = ['5th Floor'];
            window.floor5F_toppx = [329];
            window.floor5F_rightpx = [625];
            var headingtxt = "Cafeteria (Urban Chef)";
            break;
        case "radio4":
            var bsFloors = ['floor5F'],
                floorLabels = ['5th Floor'];
            window.floor5F_toppx = [307];
            window.floor5F_rightpx = [380];
            var headingtxt = "Central Sterile Supply Dispensing";
            break;
        case "radio5":
            var bsFloors = ['floor5F'],
                floorLabels = ['5th Floor'];
            window.floor5F_toppx = [148];
            window.floor5F_rightpx = [578];
            var headingtxt = "Chapel";
            break;
        case "radio6":
            var bsFloors = ['floor5F'],
                floorLabels = ['5th Floor'];
            window.floor5F_toppx = [210, 256, 193, 209, 226];
            window.floor5F_rightpx = [548, 563, 456,380,436];
            var headingtxt = "Conference Rooms";
            break;
        case "radio7":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor'];
            window.floorGF_toppx = [332,137];
            window.floorGF_rightpx = [395, 650];
            var headingtxt = "Entrances/Exits";
            break;
        case "radio8":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor', '5th Floor'];
            window.floorGF_toppx = [115,165,260];
            window.floorGF_rightpx = [600,417,395];
            var headingtxt = "Elevators";
            break;
        case "radio9":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor'];
            window.floorGF_toppx = [316];
            window.floorGF_rightpx = [307];
            var headingtxt = "Escalator";
            break;
        case "radio10":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor'];
            window.floorGF_toppx = [108];
            window.floorGF_rightpx = [566];
            var headingtxt = "MedExpress";
            break;
        case "radio11":
            var bsFloors = ['floor5F'],
                floorLabels = ['5th Floor'];
            window.floor5F_toppx = [398];
            window.floor5F_rightpx = [440];
            var headingtxt = "Pharmacy";
            break;
        case "radio12":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor'];
            window.floorGF_toppx =   [150,183,150,173,197,274,274];
            window.floorGF_rightpx = [586,586,522,522,522,313,288];
            var headingtxt = "Restaurants / Shop";
            break;
        case "radio13":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor'];
            window.floorGF_toppx = [97,114,251];
            window.floorGF_rightpx = [599, 478, 520];
            var headingtxt = "Restroom";
            break;
        case "radio14":
            var bsFloors = ['floorGF'],
                floorLabels = ['Ground Floor'];
            window.floorGF_toppx = [80,313,146,260];
            window.floorGF_rightpx = [598,323,418,340];
            var headingtxt = "Stairs";
            break;
    }  

        var speed  = 1000,
        index  = 0,
        timer  = setInterval(replaceBsFloor(index, bsFloors, floorLabels, headingtxt, contenttxt), speed),
        length = bsFloors.length;
    }

    function replaceBsFloor(index, bsFloors, floorLabels, headingtxt, contenttxt) {

        $('.bsDemo div.jqBullseye').remove();
        $('#the_leg').removeClass('floorGF floor2F floor3F floor5F floor6F floor7F floor4F floor8F floor9F floor10F floor11F floor12F floor13F floor14F floor15F');
        
        var newFloor = bsFloors[index],
            floorName = floorLabels[index],
            toppx = window[newFloor + "_toppx"],
            rightpx = window[newFloor + "_rightpx"];
         
        $('#the_leg').addClass(newFloor).find('.floor_name').text(floorName);
        
        $.each(toppx, function(ind, val){
            $('.bsDemo').bullseye({
                top : val,
                right: rightpx[ind],
                heading : headingtxt,
                content : contenttxt,
                color: "#fzff"
             });
        });

        index++;
  
        //to rotate
        if(index >= length){
           index = 0;
           clearInterval(index, 1000);
        }
        
    }
});
</script>

</body>
</html>