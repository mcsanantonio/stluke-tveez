<?php 
error_reporting(E_ALL);
include('./functions/search.php');
$result = deptList();
$department = array();
while($dept_info = $result->fetch_assoc()){
   array_push($department, $dept_info);
}

//var_dump($department);
?>

<!DOCTYPE html>
<html class="no-js" lang="en"oncontextmenu='return false;'>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>St. Luke's</title>
  <script src="assets/js/jquery-1.11.2.min.js"></script>
  <script src="assets/js/jquery.idle.js"></script>
  <script src="script.js"></script>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="assets/css/swiper.min.css">
  <style>
    .swiper-pagination-bullet {
        width: 50px;
        height: 50px;
        text-align: center;
        line-height: 50px;
        font-size: 12px;
        color:#000;
        opacity: 1;
        background: rgba(0,0,0,0.2);
        border-radius:0;
    }
	.swiper-pagination-bullet a{ color: #fff !important;}
    .swiper-pagination-bullet-active {
        color:#fff;
        background: rgba(255,255,255,0.2);
    }
  </style>
</head>
<body>
<header>
  <a id="logo" href="home.php">St. Luke's</a>
</header>
<section>
	<table border="1">
	<tr>
	<?php
		$letter = "A";
		for($i = 1; $i<= 26; $i++)
		{
		  echo '<ul class="swiper-pagination-bullet"><li><a href="#" id="'.$letter.'">'.$letter.'</a></li></ul>';
		  $letter++;
		}
	?>
	</tr>
	<tr class="dept_results">
		
	</tr>
	</table>
</section>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<nav>
  <ul>
    <li><a href="doctor.php"><span>Find a Doctor</span></a></li>
    <li class="active"><a href="services.php"><span>Find Hospital Services</span></a></li>
    <li><a href="patient.php"><span>Patient &amp; Guest Info</span></a></li>
    <li><a href="public.php"><span>Public Facilities</span></a></li>
  </ul>
</nav>
</body>

<script type="text/javascript">

	var department_list = <?php echo (count($department) > 0)? json_encode($department) : false; ?>;

</script>

<?php include('footer.php');?>