<!DOCTYPE html>
<html class="no-js" lang="en"oncontextmenu='return false;'>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>St. Luke's</title>
  <script src="assets/js/jquery-1.11.2.min.js"></script>
  <script src="assets/js/jquery.idle.js"></script>
  <script src="script.js"></script>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="assets/css/swiper.min.css">
  <style>
<?php
  require_once "kiosk_config.php";
  $db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
    if ($db->connect_error) {
      die('Connect Error (' . $db->connect_errno . ') '
      . $mysqli->connect_error);
    }
  $search = isset($_GET['search'])? addslashes($_GET['search']):"";
  $terms = explode(' ',$search);
  $bits = [];
  foreach($terms as $term){
    $bits[] = "d.destination LIKE '%".mysqli_real_escape_string($db,$term)."%'";
  }
  $search_query = "select * from kiosk02 where destination like 'A%' ";
//$search_query = "SELECT * FROM kiosk02 AS d WHERE (".implode(" AND ", $bits).") ORDER by destination ASC";

  $result = $db->query($search_query);
?>
body{font-size:11px}
#the_pagination .swiper-pagination{position:relative}
#the_pagination .swiper-pagination span{margin:0 5px 10px; background-image:url(images/letters.png);text-indent:-9999px}
#the_pagination .swiper-pagination span:nth-child(1){background-position:0 0}
#the_pagination .swiper-pagination span:nth-child(2){background-position:0 -50px}
#the_pagination .swiper-pagination span:nth-child(3){background-position:0 -100px}
#the_pagination .swiper-pagination span:nth-child(4){background-position:0 -150px}
#the_pagination .swiper-pagination span:nth-child(5){background-position:0 -200px}
#the_pagination .swiper-pagination span:nth-child(6){background-position:0 -250px}
#the_pagination .swiper-pagination span:nth-child(7){background-position:0 -300px}
#the_pagination .swiper-pagination span:nth-child(8){background-position:0 -350px}
#the_pagination .swiper-pagination span:nth-child(9){background-position:0 -400px}
#the_pagination .swiper-pagination span:nth-child(10){background-position:0 -450px}
#the_pagination .swiper-pagination span:nth-child(11){background-position:0 -500px}
#the_pagination .swiper-pagination span:nth-child(12){background-position:0 -550px}
#the_pagination .swiper-pagination span:nth-child(13){background-position:0 -600px}
#the_pagination .swiper-pagination span:nth-child(14){background-position:0 -650px}
#the_pagination .swiper-pagination span:nth-child(15){background-position:0 -700px}
#the_pagination .swiper-pagination span:nth-child(16){background-position:0 -750px}
#the_pagination .swiper-pagination span:nth-child(17){background-position:0 -800px}
#the_pagination .swiper-pagination span:nth-child(18){background-position:0 -850px}
#the_pagination .swiper-pagination span:nth-child(19){background-position:0 -900px}
#the_pagination .swiper-pagination span:nth-child(20){background-position:0 -950px}
#the_pagination .swiper-pagination span:nth-child(21){background-position:0 -1000px}
#the_pagination .swiper-pagination span:nth-child(22){background-position:0 -1050px}
#the_pagination .swiper-pagination span:nth-child(23){background-position:0 -1100px}
#the_pagination .swiper-pagination span:nth-child(24){background-position:0 -1150px}
#the_pagination .swiper-pagination span:nth-child(25){background-position:0 -1200px}
#the_pagination .swiper-pagination span:nth-child(26){background-position:0 -1250px}
    .swiper-pagination-bullet {
	    width: 50px;
        height: 50px;
        text-align: center;
        line-height: 50px;
        font-size: 12px;
        color:#000;
        opacity: 1;
        background: rgba(0,0,0,0.2);
		border-radius:0;
    }
    .swiper-pagination-bullet-active {
        color:#fff;
        background: rgba(255,255,255,0.2);
    }
	.inner_page .btn-set1{text-align:left}
	.inner_page .btn-set1 li a{text-align:center}
  </style>
</head>
<body>
<header>
  <a id="logo" href="home.php">St. Luke's</a>
</header>
<section>
  <h1>Center & departments</h1>
  <h3 id="records">Number of records found : <span><?php echo $result->num_rows?></span></h3>
  <div id="the_pagination">
    <div class="swiper-pagination"></div>
  </div>
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
	    <div class="inner_page">
	<ul class="btn-set1">
<?php if($result->num_rows > 0) :?>
  <?php $counter = 1;?>
    <?php while($kiosk02 = $result->fetch_object()):?>
	  <li><a href="map-admission.php?id=<?php echo $kiosk02->id?>"><?php echo "{$kiosk02->destination}" ?></a></li>
      <?php endwhile;?>
      <?php else: ?>
      No record found.
      <?php endif;?>
	</ul>

    <?php $db->close(); ?>
<!--		
		
		  <ul class="btn-set1">
	        <li><a href="sample.php" class="" >Acute Stroke Unit</a></li>
	        <li><a href="sample.php" class="" >Aesthetic Center</a></li>
	        <li><a href="sample.php" class="" >Aesthetic Surgery</a></li>
	        <li><a href="sample.php" class="" >Allergology and Immunology</a></li>
	        <li><a href="sample.php" class="" >Anesthesiology</a></li>
		  </ul>
		  -->
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	        <li><a href="sample.php" class="" >Bone Marrow Transplant Unit</a></li>
	        <li><a href="sample.php" class="" >Breast Clinic</a></li>
		  </ul>
        </div>
      </div>

	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Cancer Center</a></li>
	  <li><a href="sample.php" class="" >Cardiac Catheterization Laboratory</a></li>
	  <li><a href="sample.php" class="" >Cardiology</a></li>
	  <li><a href="sample.php" class="" >Cellular Therapeutics Center</a></li>
	  <li><a href="sample.php"  >Center for Tropical and Travel Medicine (CTTM)</a></li>
	  <li><a href="sample.php" class="" >Clinical Psychology</a></li>
	  <li><a href="sample.php" class="" >CLS Eye Center</a></li>
	  <li><a href="sample.php" class="" >Colorectal Surgery</a></li>
	  <li><a href="sample.php" class="" >Computerized Tomography (CT Scan)</a></li>
	  <li><a href="sample.php" class="" >Critical Care / Intensive Care</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Delivery Room</a></li>
	  <li><a href="sample.php" class="" >Department of Medicine</a></li>
	  <li><a href="sample.php" class="" >Dermatology</a></li>
	  <li><a href="sample.php" class="" >Diabetes Care Center</a></li>
		  </ul>
        </div>
      </div>

	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Emergency Medicine</a></li>
	  <li><a href="sample.php" class="" >Endocrinology</a></li>
	  <li><a href="sample.php" class="" >Endoscopy Unit</a></li>
	  <li><a href="sample.php" class="" >ENT Diagnostic Center</a></li>
		  </ul>
        </div>
      </div>
	
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	      <li><a>content start at letter F not found</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Gastroenterology</a></li>
	  <li><a href="sample.php" class="" >General Medicine</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Heart Station</a></li>
	  <li><a href="sample.php" class="" >Hematology</a></li>
	  <li><a href="sample.php" class="" >Home Care and Hospice</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Infectious Diseases</a></li>
	  <li><a href="sample.php" class="" >Institutional Review Board</a></li>
	  <li><a href="sample.php" class="" >Interventional Radiology</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	      <li><a>content start at letter J not found</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	      <li><a>content start at letter K not found</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	        <li><a href="sample.php"  >Liver Care Center (Hepatobiliary and Pancreatic Center)</a></li>
		  </ul>
        </div>
      </div>

	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Magnetic Resonance Imaging (MRI)</a></li>
	  <li><a href="sample.php" class="" >MakatiMed Health Hub</a></li>
	  <li><a href="sample.php" class="" >MakatiMed Health Hub (in-patient)</a></li>
	  <li><a href="sample.php" class="" >MCF DermLaser and Phototherapy</a></li>
	  <li><a href="sample.php" class="" >Memory Plus Center</a></li>
		  </ul>
        </div>
      </div>

	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Neonatal ICU</a></li>
	  <li><a href="sample.php" class="" >Nephrology</a></li>
	  <li><a href="sample.php" class="" >Neurological Sciences</a></li>
	  <li><a href="sample.php" class="" >Neurology</a></li>
	  <li><a href="sample.php"  >Neurophysiology and Sleep Disorders Laboratory</a></li>
	  <li><a href="sample.php" class="" >Neurosurgery</a></li>
	  <li><a href="sample.php" class="" >Nuclear Medicine</a></li>
	  <li><a href="sample.php" class="" >Nursing Services Division</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Obstetrics and Gynecology</a></li>
	  <li><a href="sample.php" class="" >Oncology</a></li>
	  <li><a href="sample.php" class="" >Oncology Unit</a></li>
	  <li><a href="sample.php" class="" >Operating Rooms</a></li>
	  <li><a href="sample.php" class="" >Ophthalmology</a></li>
	  <li><a href="sample.php" class="" >Optimal Aging Center</a></li>
	  <li><a href="sample.php" class="" >Orthopaedics</a></li>
	  <li><a href="sample.php" class="" >Osteoporosis and Bone Health</a></li>
	  <li><a href="sample.php"  >Otorhinolaryngology (Ear, Nose, and Throat)</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Pain Control Clinic</a></li>
	  <li><a href="sample.php" class="" >Pathology and Laboratories</a></li>
	  <li><a href="sample.php" class="" >Pediatric Intensive Care Unit (PICU)</a></li>
	  <li><a href="sample.php" class="" >Pediatric Surgery</a></li>
	  <li><a href="sample.php" class="" >Pediatrics</a></li>
	  <li><a href="sample.php" class="" >Pharmacy Services</a></li>
	  <li><a href="sample.php" class="" >Physical Medicine and Rehabilitation</a></li>
	  <li><a href="sample.php" class="" >Plastic & Reconstructive Surgery</a></li>
	  <li><a href="sample.php" class="" >Psychiatry</a></li>
	  <li><a href="sample.php" class="" >Pulmonary Diseases</a></li>
	  <li><a href="sample.php" class="" >Pulmonary Laboratory</a></li>
		  </ul>
        </div>
      </div>
	  
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	      <li><a>content start at letter Q not found</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Radiation Oncology</a></li>
	  <li><a href="sample.php" class="" >Radiology</a></li>
	  <li><a href="sample.php" class="" >Renal Care Services</a></li>
	  <li><a href="sample.php" class="" >Rheumatology</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Surgery</a></li>
	  <li><a href="sample.php" class="" >Surgical ICU</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Telemetry</a></li>
	  <li><a href="sample.php" class="" >Thoracic Cardiovascular</a></li>
	  <li><a href="sample.php" class="" >TomoTherapy Radiation Treatment</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Ultrasound</a></li>
	  <li><a href="sample.php" class="" >Urogynecology and Incontinence Center</a></li>
	  <li><a href="sample.php" class="" >Urology</a></li>
		  </ul>
        </div>
      </div>

	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	      <li><a>content start at letter V not found</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >Weight Wellness Center</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	  <li><a href="sample.php" class="" >X-Ray</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	      <li><a>content start at letter Y not found</a></li>
		  </ul>
        </div>
      </div>
	  
	  <div class="swiper-slide">
	    <div class="inner_page">
		  <ul class="btn-set1">
	      <li><a>content start at letter Z not found</a></li>
		  </ul>
        </div>
      </div>
    </div>
  </div>


</section>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<nav>
  <ul>
    <li><a href="doctor.php"><span>Find a Doctor</span></a></li>
    <li class="active"><a href="services.php"><span>Find Hospital Services</span></a></li>
    <li><a href="patient.php"><span>Patient &amp; Guest Info</span></a></li>
    <li><a href="public.php"><span>Public Facilities</span></a></li>
  </ul>
</nav>

    <script src="assets/js/swiper.jquery.min.js"></script>
    <script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        paginationBulletRender: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
        }
    });
    </script>
</body>
</html>      