<?php
error_reporting(E_ALL);
include('header.php');
include('./functions/search.php');
$result = doctorsList();
$doctors = array();
while($doctor_info = $result->fetch_assoc()){
   array_push($doctors, $doctor_info);
}
?>
<?php include('header.php');?>
<body id="the_frame" class="link1">
<header>
  <a id="logo" href="home.php">St. Lukes</a>
</header>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<?php include('nav.php');?>


<section>
  <h1>Find a Doctor - By Specialization</h1>
        <div id="searchtxt-wrapper">
             <input type="text" id="search_txt" data-type='specialization' placeholder="Type specialization"  />
             <div class="auto-suggest" id="auto-suggest"></div>  
        </div>
      <div id="keyboardcont" onmousedown='return false;'>
	  
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">1</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">2</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">3</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">4</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">5</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">6</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">7</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">8</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">9</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">0</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">-</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue del"><i class="fa fa-arrow-left"></i></button></span>
        <br>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">q</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">w</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">e</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">r</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">t</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">y</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">u</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">i</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">o</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">p</button></span>
        <span class="the_spacer"></span>
        <br>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">a</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">s</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">d</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">f</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">g</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">h</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">j</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">k</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">l</button></span>
        <span class="the_spacer"></span>
        <br>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">z</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">x</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">c</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">v</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">b</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">n</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">m</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">,</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">.</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">ñ</button></span>
        <br>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue space">Space</button></span>

        <form name="frmbyname" action="specialization_list.php" id="search_doctor" method="GET">
          <span><button onclick="clicksound.playclip()" class="action-button animate blue search" name="search">Search</button></span>
        </form>
    </div>	
</section>
<script type="text/javascript">

    var doctor_list = <?php echo (count($doctors) > 0)? json_encode($doctors) : false; ?>;

    $(function(){
        /*Search Doctor*/
        $("#search_doctor").on("submit",function(){
            var search_val = $("#search_txt").val();
            var input = $("<input>").attr({"type"  : "hidden" , "name" : "search"}).val(search_val);
            $(this).append($(input));
            return true;
        });

        /*Keyboard Functionalities*/
        $(".action-button").on("click",function(){
            var container = $("#search_txt");
            var current_value = container.val();
            switch(($(this).text()).toLowerCase()){
                case "space" : 
                    current_value += " ";
                break;
                case "search" : 
                    current_value += "";
                break;
                case "back" : 
                    current_value += "";
                break;
                case "help" : 
                    current_value += "";
                break;
                case "":
                    var icon = $(this).children('i');
                    if(icon.hasClass("fa-arrow-left")){
                        var len = current_value.length;
                        current_value  = current_value.substr(0,len-1);
                    }
                break;
                default : 
                current_value += $(this).text();
            }
            container.val(current_value);
            $('#search_txt').trigger('change');
        });
    });
</script>
<?php include('footer.php');?>
