<?php include('header.php');?>
<style>
.btn-set1 li{text-align:center}
</style>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Patient & Guest Info</h1>
<ul class="btn-set1" id="color-item" style="text-align:left">
  <li><a href="patient-inner01.php">Admission Procedures</a></li>
  <li><a href="patient-inner02.php">Accommodations</a></li>
  <li><a href="patient-inner03.php">HMOs</a></li>
  <li><a href="patient-inner04.php">Visiting Hours</a></li>
  <li><a href="patient-inner05.php">Patient Services</a></li>
  <li><a href="patient-inner06.php">Medical Records</a></li>
  <li><a href="patient-inner07.php">Billing & Payment</a></li>
  <li><a href="patient-inner08.php">Discharge Procedures</a></li>
  <li><a href="patient-inner09.php">Concierge</a></li>
  <li><a href="patient-inner10.php">Hotel</a></li>
  <li><a href="patient-inner11.php">International Patient Care</a></li>
  <li><a href="patient-inner12.php">Emergency Services</a></li>
  <li><a href="patient-inner13.php">Shops and Restaurants</a></li>
</ul>
</section>
<?php include('footer.php');?>