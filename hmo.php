<?php
  include('header.php');
  include('functions/search.php');

  $search = isset($_GET['id'])? addslashes($_GET['id']) : "";
  //$search = trim($_GET['id']);

  $result = search("hmo_info","hmo_name",$search);
?>

<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Find a doctor - by HMO</h1>
<ul class="btn-set1 set2">
<?php if($result->num_rows > 0) :?>
  <?php $counter = 1;?>
  <?php while($hmo_info = $result->fetch_object()):?>
  <li><a href="hmo_info.php?id=<?php echo $hmo_info->id?>"><?php echo "{$hmo_info->hmo_name}" ?></a></li>
      <?php endwhile;?>
      <?php else: ?>
      <li>No record found.</li>
      <?php endif;?>
</ul>
</section>
<?php include('footer.php');?>