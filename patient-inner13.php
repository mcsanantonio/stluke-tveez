<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Shop and Restaurant</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <div class="one-third row">
    <div class="the_inner no-minheight">
  <h2>Dining and Shopping Facilities</h2>
  <p>A wide variety of dining choices is available to family and guests while at St. Luke's. Below are several establishments located within our premises, offering delightful culinary specialties, well-loved favorites as well as essentials and gift items: </p>
  <ul class="bull">
    <li>Bizu</li>
    <li>Dairy Queen</li>
    <li>Market on the 5th</li>
    <li>Mary Grace</li>
    <li>Pizza Hut</li>
    <li>Rustan's Flower Shop</li>
    <li>Starbucks</li>
    <li>Via Mare</li>
  </ul>
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner no-minheight">
  <h2>St. Luke's Gift Shop</h2>
For unique gifts and novelties, the St. Luke's Gift Shop offers a wide variety of merchandise that proudly carries the St. Luke's Medical Center brand. From caps and jackets, quality leather goods, jewelry and baby apparel, patients and guests will delight in the wide assortment of goods at reasonable prices. 
The St. Luke's Gift Shop is located at the main lobby and is open from 9:30 AM to 7:00 PM (Monday to Friday) and 10:00 AM to 3:00 PM (Saturday). For inquiries, call (632) 7897700 ext. 1098.
    </div>
  </div>
  <div class="one-third row">
    <div class="the_inner no-minheight">
  <h2>Banks</h2>
For the convenience of patients, guests and visitors, a number of major banks with ATMs can be found inside the hospital premises.
  <ul class="bull">
    <li>BDO</li>
    <li>Chinabank (ATM only)</li>
    <li>Security Bank</li>
  </ul>
</div>
    </div>
</div>
</div>
</section>
<?php include('footer.php');?>