var html5_audiotypes={ 
	"mp3": "audio/mpeg",
	"mp4": "audio/mp4",
	"ogg": "audio/ogg",
	"wav": "audio/wav"
}

function createsoundbite(sound){
	var html5audio=document.createElement('audio')
	if (html5audio.canPlayType){ 
		for (var i=0; i<arguments.length; i++){
			var sourceel=document.createElement('source')
			sourceel.setAttribute('src', arguments[i])
			if (arguments[i].match(/\.(\w+)$/i))
				sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
			html5audio.appendChild(sourceel)
		}
		html5audio.load()
		html5audio.playclip=function(){
			html5audio.pause()
			html5audio.currentTime=0
			html5audio.play()
		}
		return html5audio
	}
	else{
		return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
	}
}

var clicksound=createsoundbite("click.ogg", "click.mp3")

/**
 * Search and auto-suggest
 */

$(document).ready(function() {
    var minlength = 3;

    $("#search_txt").on("change keyup paste", function(){
        var value = $('#search_txt').val(),
            dType = $('#search_txt').attr('data-type');
            max_doc = 2,
            cur_doc = 0,
            suggestHtml = [];
            
	
        $('#auto-suggest').empty().hide();            
        $.each(doctor_list, function(ind, val){

            var name = val.surname + ' ' + val.firstname + " " + val.suffix,
                spec = val.specialization,
                proc = val.test_proc_name;
            
            if(dType=="specialization"){
                var span_txt = spec;
            }else if(dType=="test_proc_name"){
                var span_txt = proc;
            }else{
                var span_txt = name;
            }
            
            span_txt = span_txt.toUpperCase();
            var surUpper = span_txt,
                value_to_search = value.toUpperCase();
            if(value!="" && surUpper.search(value_to_search) > -1 && cur_doc < max_doc){ //surUpper.lastIndexOf(value.toUpperCase(), 0) === 0

               span_txt = span_txt.replace(value_to_search, "<b style='background-color: #AAD1B7'>" + value_to_search + "</b>");
                
               if(dType=="specialization"){
                   var autosughtml = '<span class="name">'+ name +'</span>, <span class="detail">'+span_txt+'</span>';
               }else if(dType=="test_proc_name"){
                   var autosughtml = '<span class="detail">'+span_txt+'</span>';   
               }else{
                   var autosughtml = '<span class="name">'+ span_txt +'</span>, <span class="detail">'+spec+'</span>';
               }

               suggestHtml.push('<div><a href="doctor_info.php?id='+val.id+'">'+autosughtml+'</a></div>');
               cur_doc++;
               console.log(val);
            }
        });

        $('#auto-suggest').empty().html(suggestHtml.join(''));
        if(suggestHtml.length > 0){
            $('#auto-suggest').show();
        }else{
            $('#auto-suggest').hide();
        }
        cur_doc = 0;
        suggestHtml = [];
        
    });
	
	$("#procedure_search").on("change keyup paste", function(){
        var value = $('#procedure_search').val(),
            dType = $('#procedure_search').attr('data-type');
            max_doc = 2,
            cur_doc = 0,
            suggestHtml = [];
			
        $('#auto-suggest').empty().hide();            
        $.each(procedure_list, function(ind, val){
            var surUpper = val[dType].toUpperCase();
            if(value!="" && surUpper.search(value.toUpperCase()) > -1 && cur_doc < max_doc){
 
               var span_txt = val.test_proc_name.replace(value.toUpperCase(), "<b style='background-color: #AAD1B7'>" + value.toUpperCase() + "</b>");
             
               suggestHtml.push('<div><a href="map-main.php?id='+val.id+'">'+ span_txt +'</a></div>');
               cur_doc++;

            }
        });

        $('#auto-suggest').empty().html(suggestHtml.join(''));
        if(suggestHtml.length > 0){
            $('#auto-suggest').show();
        }else{
            $('#auto-suggest').hide();
        }
        cur_doc = 0;
        suggestHtml = [];
        
    });
	
	$(".swiper-pagination-bullet a").on("click", function(e) {
		cur_doc = 0,
		$('.dept_results').empty();

		$.each(department_list, function(ind, val){
			var value = $(e.target).text();
            var surUpper = val['center_dept_name'].toUpperCase();
			
            if(value!="" && surUpper.lastIndexOf(value.toUpperCase(), 0) === 0){
				$('.dept_results').html('<td><a href="map-main.php?id='+val.map_id+'">'+ val.center_dept_name +'</a></td>');
				cur_doc++;
            }
        });
		
		cur_doc = 0;
	});
});
