<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Billing & Payment</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Billing </h2>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Room and Board</h3>
  <p>Room and board is charged at full rate during the first day of confinement regardless of the time the patient is admitted. Upon discharge, a patient is billed half (1/2) day if discharged after 11:00 AM but before 5:00 PM and a full (1) day if discharged after 5:00 PM.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Other Charges</h3>
  <p>Pharmacy and medical supplies used and procedures performed are itemized in the statement of account on a daily basis.</p>
    </div>
  </div>
  <br style="clear:both">
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Special Packages</h3>
  <p>Executive Check-Up (ECU) – Various wellness programs are designed to provide a baseline measure of a patient's health. The patient may select one program that will focus only on a particular health issue he is most concerned about.</p>
  <p>Cardiac Surgery Packages are offered to patients who will undergo open heart surgeries. These are also designed to offer affordable and competitive surgery prices to patients. As packages, these come in fixed rates but are subject to certain limitations on coverage and manner of payment.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Receipt of Statement of Account/Update of Bill during Confinement</h3>
  <p>Patients are encouraged to review their statement of account on a regular basis prior to discharge. The statement of account contains details of the charges to the patient on room and board, medicines and supplies used and procedures performed.</p>
    </div>
  </div>
  <hr>
  <h2>Payments</h2>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Private Paying Patients</h3>
  <p>Patients without insurance will be requested to make an initial payment upon admission. The In-House Collection Office will request for additional payments during confinement and full payment upon discharge.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">HMOs and Corporate Accounts</h3>
  <p>A Letter of Authorization (LOA) from accredited companies and Health Maintenance Organizations (HMOs) should be submitted upon admission to avoid initial request for payments. Should the Letter of Authorization indicate a maximum coverage limit, the excess of the limit will be paid by the patient during confinement or prior to his discharge from the hospital. The Billing Help Desk Office and In-House Collection Office will assist all patients in coordinating with HMOs and Corporations for the letter of guarantee and additional documentation requirements.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Manner of Payment</h3>
  <p>The hospital accepts only cash or credit cards for payments. Personal checks may be accepted only for initial payments or payment of progress bills during confinement. Patients without companions may seek the assistance of the hospital staff if they need to go to the bank for their funds requirements.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">PhilHealth</h3>
  <p>PhilHealth deductions will be reflected on the final hospital bill of the patient as long as the necessary supporting documents required by PhilHealth are submitted prior to discharge. PhilHealth forms may be secured from the Admission Office, In-House Collection Office or at the Billing Office. Please refer to the detailed PhilHealth guidelines provided to the patient upon admission.</p>
    </div>
  </div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h3 class="space">Professional Fees of Doctors</h3>
  <p>To facilitate discharge, patients are encouraged to discuss professional fees with their doctors upon admission and during confinement.</p>
  <p>Doctor's professional fees are billed separately from the hospital charges. Doctors may choose from the following types of payment of professional fees:</p>
  <ul class="no-bull">
    <li>Direct settlement – the patient pays the fees directly to the doctors.</li>
    <li>Collect – the patient pays the fees to the Hospital Cashier together with the hospital bill.</li>
  </ul>
</div>
</div>
</div>
</div>
</section>
<?php include('footer.php');?>