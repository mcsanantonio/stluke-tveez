<?php
error_reporting(E_ALL);
// Excel reader from http://code.google.com/p/php-excel-reader/
require('php-excel-reader/excel_reader2.php');
require('SpreadsheetReader.php');


// Connect to db
$db_host = "localhost";
$db_user = "root";
$db_pass = "";
$db_name = "tveezph_kiosk";

@mysql_connect("$db_host","$db_user","$db_pass") or die ("Could not connect to MYSQL");
@mysql_select_db("$db_name") or die("No database");

$db = new mysqli($db_host, $db_user,$db_pass,$db_name, "3306"); //port is a string!
if ($db->connect_error) {
    die('Connect Error (' . $db->connect_errno . ') '. $mysqli->connect_error);
}


$target_dir = "./uploads/";
$uploadOk = 1;
$msg = "";

// Check if image file is a actual image or fake image
if(isset($_POST["submit"]) && !empty($_POST['t_name'])) {
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$filetype = pathinfo($target_file,PATHINFO_EXTENSION);
    if($filetype == "xls" || $filetype=="xlsx") {
        $uploadOk = 1;
    } else {
        $msg = "file not an excel";
        $uploadOk = 0;
    }

    if($uploadOk==1){
        
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
/*---- START EXCEL READING---*/ 
        // INSERT UPLOADED FILE
        $insert_query = "INSERT INTO uploads_info (table_name, file_name) VALUES ('{$_POST['t_name']}', '{$_FILES["fileToUpload"]["name"]}')";
        $db->query($insert_query);


try{
        $Filepath = $target_file;
        $Spreadsheet = new SpreadsheetReader($Filepath);
        $BaseMem = memory_get_usage();
        $Sheets = $Spreadsheet->Sheets();

        $t_fields = array(
            'doctor_info' => array('doctor_id', 'surname', 'firstname', 'middle', 'suffix', 'specialization', 'specialty'),
            'hmo_info'    => array('hmo_name', 'room', 'local', 'directline', 'coordinator', 'dayshours'),
            'kiosk02'     => array('path', 'path2', 'floor', 'destination', 'step1', 'step2', 'step3', 'step4')
        );

        $t_values = array();

        $table_name = explode(':', $_POST['t_name']);

              

        $Spreadsheet = new SpreadsheetReader($target_file); 
        $Sheets = $Spreadsheet->Sheets(); 
        foreach ($Sheets as $Index => $Name)
        {
            $Time = microtime(true);
            $Spreadsheet->ChangeSheet($Index);
            foreach ($Spreadsheet as $Key => $Row)
            {
                if(isset($table_name[1]) && $table_name[1]=='updateclinic'){
                    // update doctor_info
                }elseif(isset($t_fields[$table_name[0]])){
                    if($Key > 0 && !empty($Row[0]) && !empty($Row[1])){
                        switch($table_name[0]){
                            case 'doctor_info':
                                $fname = str_replace("'", "\'", $Row[2]);
                                $lname = str_replace("'", "\'", $Row[1]);
                                $push = "('{$Row[0]}', '{$lname}', '{$fname}', '{$Row[3]}', '{$Row[4]}', '$Row[5]', '$Row[6]')";
                                break;
                            case 'hmo_info':
                                $push = "('{$Row[2]}', '{$Row[3]}', '{$Row[4]}', '{$Row[5]}', '{$Row[6]}', '{$Row[7]}')";
                                break;
                            case 'kiosk02':
							    $service = str_replace("'", "\'", $Row[1]);
								$s1 = str_replace("'", "\'", $Row[5]);
								$s2 = str_replace("'", "\'", $Row[6]);
								$s3 = str_replace("'", "\'", $Row[7]);
								$s4 = str_replace("'", "\'", $Row[8]);
						        $push = "('{$Row[3]}', '{$Row[4]}', '{$Row[2]}', '{$service}', '{$s1}', '{$s2}', '{$s3}', '{$s3}')";
                                break;
                            default:
                                $push = '';
                         }
                         array_push($t_values, $push);
                    }
                }
            }

            // apply query
            if(isset($table_name[1]) && $table_name[1]=='clinic'){
                // update doctor_info
            }elseif($table_name[0] && count($t_values) > 0){
                // truncate
                $db->query("truncate {$table_name[0]}");
                //insert
                $values = implode(',', $t_values);
                $fields = implode(',', $t_fields[$table_name[0]]);
				
                $db->query("INSERT INTO {$table_name[0]} ({$fields}) VALUES {$values}");
            }
        }
}catch (Exception $E){
    echo $E->getMessage();
}


/*---- END READING ---*/
        } else {
            $msg =  "error in uploading";
        }
    }

}



?><!DOCTYPE html>
<html>
<head>
<style>
.collapse {
	border-collapse: collapse;
	border: 1px solid gray;
}

.collapse td {
	padding: 5px;
	border: 1px solid black;
}

</style>

<body>



<form action="index.php" method="post" enctype="multipart/form-data">
    <table>
        <tr><td colspan="2"><h3 style="color: red;"><?php echo $msg; ?></h3></td></tr>
        <tr>
            <td>Select table:</td>
            <td>
                <select name="t_name">
                    <option value="">-- Please select table --</option>
                    <option value="doctor_info">Doctor</option>
                    <option value="hmo_info">HMO</option>
                    <option value="kiosk02">KIOSK</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Select excel to upload:</td>
            <td><input type="file" name="fileToUpload" id="fileToUpload"></td>
        </tr>
        <tr><td colspan="2" align="right"><input type="submit" value="Upload" name="submit"></td></tr>
    </table>
</form>
<hr/>
<h3>Uploaded Files</h3>
<table class="collapse">
    <tr> 
        <th>Table</th>
        <th>File</th>
        <th>Date</th>
    </tr>
<?php 
// SELECT ALL UPLOAD
$uploads_query = "SELECT * FROM uploads_info";
$result = $db->query($uploads_query);
while($info = $result->fetch_assoc()){
?>
    <tr>
        <td><?php echo $info['table_name']; ?></td>
        <td><?php echo $info['file_name']; ?></td>
        <td><?php echo $info['created_at']; ?></td>
    </tr>
<?php } ?>
</table>

</body>
</html>
