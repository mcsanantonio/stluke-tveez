<!doctype html>
<html class="no-js" lang="en"oncontextmenu='return false;'>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
  <title>St. Luke's</title>
  <style>
    @import url('assets/css/swiper.min.css');
  </style>
  <link rel="stylesheet" href="assets/source/jquery.fancybox.css?v=2.1.5" />
  <link rel="stylesheet" href="style.css">
  <style>
    .swiper-container {width: 100%;height: 1080px;margin: 0;}
    .swiper-slide {text-align: center;font-size: 18px;background: #fff;
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
  </style>
  <script src="assets/js/jquery-1.11.2.min.js"></script>
  <script src="assets/js/swiper.min.js"></script>
  <script src="assets/source/jquery.fancybox.js"></script>
  <script src="js/moment.min.js"></script>
  <script>
  $(document).ready(function() {
    var interval = setInterval(function() {
        var momentNow = moment();
        $('#date-part').html(momentNow.format('MMMM DD, YYYY') + ' '
                            + momentNow.format('dddd')
                             .substring(0,3).toUpperCase());
        $('#time-part').html(momentNow.format('hh:mm:ss A'));
    }, 100);
    
    $('#stop-interval').on('click', function() {
        clearInterval(interval);
    });
});
  </script>
</head>
<body>
<aside id="ads">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <div class="swiper-slide" id="ads1"><a class="fancybox" data-fancybox-group="ads" href="ads/1b.jpg"><img src="ads/1.jpg" alt=""></a></div>
      <div class="swiper-slide" id="ads2"><a class="fancybox" data-fancybox-group="ads" href="ads/2b.jpg"><img src="ads/2.jpg" alt=""></a></div>
      <div class="swiper-slide" id="ads3"><a class="fancybox" data-fancybox-group="ads" href="ads/3b.jpg"><img src="ads/3.jpg" alt=""></a></div>
    </div>
  </div>
</aside>
<div id="time">
  <div class='time-frame'>
    <div id='date-part'></div>
    <div id='time-part'></div>
  </div>
</div>
<iframe src="home.php"></iframe>
<script>
    var mySwiper = new Swiper('.swiper-container', {
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: 8000,
        autoplayDisableOnInteraction: false,
        loop: true,
});   
$(document).ready(
    function() {
        $("#ads1").click(function() {
            $("#cont_ads1").fadeToggle();
        });
        $("#ads2").click(function() {
            $("#cont_ads2").fadeToggle();
        });
        $("#ads3").click(function() {
            $("#cont_ads3").fadeToggle();
        });
    });

	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
</body>
</html>      