<?php
  include('header.php');
  include('functions/search.php');

  $search = isset($_GET['search'])? addslashes($_GET['search']) : "";
  $search = trim($_GET['search']);

  $result = search("doctor_info","surname",$search,"surname");

  //echo "<pre>";print_r($result);echo "</pre>";
?>

<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Doctors' list</h1>
  <div id="records">Number of records found : <span><?php echo $result->num_rows?></span></div>
  <ul class="doclist" id="doclist_title">
	<li>&nbsp;</li>
	<li>Doctor's Name</li>
	<li>Specialization</li>
  </ul>
  <div class="bg_area" id="all_list">
<?php if($result->num_rows > 0) :?>
  <?php $counter = 1;?>
    <?php while($doctor_info = $result->fetch_object()):?>
      	<a href="doctor_info.php?id=<?php echo $doctor_info->id?>" style="padding: 10px; <?php echo $counter++%2 == 1 ? 'background: #FFFFFF;' : 'background: #E3F2FD;' ?>">
      	<ul class="doclist">
      	  <li class="view">View Info</li>
      	  <li><?php echo "{$doctor_info->surname}, {$doctor_info->firstname} {$doctor_info->middle}" ?><?php if(!empty($doctor_info->suffix)) { echo ", {$doctor_info->suffix}"; } ?></li>
      	  <li><?php echo "{$doctor_info->specialization}" ?></li>
      	</ul>
      	</a>
      <?php endwhile;?>
      <?php else: ?>
      No record found.
      <?php endif;?>


  </div>
</section>
<?php include('footer.php');?>
