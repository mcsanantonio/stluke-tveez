<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Hotel</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>7 South and 10 North Place</h2>
  <p>For patients’ companions, St. Luke’s offers guest rooms at 7 South and 10 North Place with hotel-like amenities. Each room is equipped with air conditioning, cable TV, DVD player, Wi-Fi Access and a safety deposit box. It is a comfortable home away from home while attending to the needs of loved ones confined at the hospital. </p>
  <h2>Available rooms:</h2>
  <div class="one-fifth row">
    <div class="the_inner no-minheight">
    <h3 class="space">Regular Private</h3>
	<img class="responsive" src="images/hotel-regular-private.jpg">
</div>
</div>
  <div class="one-fifth row">
    <div class="the_inner no-minheight">
    <h3 class="space">Deluxe Private</h3>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
</div>
</div>
  <div class="one-fifth row">
    <div class="the_inner no-minheight">
    <h3 class="space">Semi-Deluxe</h3>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
</div>
</div>
  <div class="one-fifth row">
    <div class="the_inner no-minheight">
    <h3 class="space">Regular Suite</h3>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
</div>
</div>
  <div class="one-fifth row">
    <div class="the_inner no-minheight">
    <h3 class="space">Suite Room</h3>
	<img class="responsive" src="images/hotel-deluxe-private.jpg">
  </ul>
</div>
</div>
</div>
</div>
</section>
<?php include('footer.php');?>