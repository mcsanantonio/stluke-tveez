<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>International Patient Care</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>One-Stop Service</h2>
  <p>You and your family can rely on our friendly and knowledgeable staff to make your stay at St. Luke's a most successful one. International Patient Care is a full-service department dedicated to meeting your needs and requirements.</p>
</div>
</div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Medical Referrals and Schedule of Appointments</h2>
  <p>Depending on your needs, IPC staff arranges an appointment with your preferred medical specialist and scheduling of diagnostic tests/procedures.</p>
</div>
</div>
<br class="clearfix">
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Hospital Admission</h2>
  <p>We assist you in planning your stay at the hospital, and assist you from the time of admission until discharge.</p>
</div>
</div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Travel Planning</h2>
  <p>To ensure a positive experience, we provide logistical support for your travel requirements- from airline ticketing, land transportation, to room reservations- in coordination with the Concierge Department.</p>
</div>
</div>
<br class="clearfix">
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Transportation Arrangements</h2>
  <p>From your arrival at the airport, we may provide transfer services to and from St. Luke's via hospital transport service, ambulance or taxi.</p>
</div>
</div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Insurance Facilitation</h2>
  <p>We can assist in coordinating with the patient's insurance company for concerns in settlement of payment.</p>
</div>
</div>
<br class="clearfix">
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Interpreter Service</h2>
  <p>We assist in coordinating with outsourced interpreters as needed.</p>
</div>
</div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Business Center Use</h2>
  <p>We provide computer usage with internet access, photocopy, scanning and printing services for our guests.</p>
</div>
</div>
<br class="clearfix">
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Hospital Assistance</h2>
  <p>We coordinate with your attending physician and assist you in navigating the healthcare system to get the most out of your experience.</p>
</div>
</div>
  <div class="one-half row">
    <div class="the_inner no-minheight">
  <h2>Ambulance Service</h2>
  <p>In coordination with the Emergency Care Department, ambulance conduction within Metro Manila can be made available for patients who have to be transported to and from St. Luke's. Relatives of patients may coordinate with the department at ext.1078</p>
  <p>The International Patient Care Department is open Mondays to Saturdays from 8:00 AM to 5:00 PM. IPC is a one stop shop for all you needs.</p>
</div>
</div>

</div>
</div>
</section>
<?php include('footer.php');?>