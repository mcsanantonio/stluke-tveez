<?php 

include('header.php');
include('./functions/search.php');
$result = doctorsList();
$doctors = array();
while($doctor_info = $result->fetch_assoc()){
   array_push($doctors, $doctor_info);
}

?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Find a Doctor - By Name</h1>
      <div id="searchtxt-wrapper">
	  <input type="text" id="search_txt" data-type='surname' value=""  />
         <div class="auto-suggest" id="auto-suggest"></div>
      </div>
      <div id="keyboardcont" onmousedown='return false;'>
        <span><button title="q" onclick="clicksound.playclip()" class="action-button animate blue">q</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">w</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">e</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">r</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">t</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">y</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">u</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">i</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">o</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">p</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="width: 175px;"><i class="fa fa-arrow-left"></i></button></span>
  
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="margin:0 10px 0 52px">a</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">s</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">d</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">f</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">g</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">h</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">j</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">k</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">l</button></span>
		
        <form name="frmbyname" action="doctor_name_list.php" id="search_doctor" method="GET">
          <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="width: 240px;" name="search">Search</button></span>
        </form>
		
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="margin:0 10px 0 102px">z</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">x</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">c</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">v</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">b</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">n</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">m</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">.</button></span>
        <span><button onclick="clicksound.playclip()" class="action-button animate blue">-</button></span>
           
        <span><button onclick="clicksound.playclip()" class="action-button animate blue" style="width: 1280px;">Space</button></span>
    </div>	
</section>
<script type="text/javascript">

    var doctor_list = <?php echo (count($doctors) > 0)? json_encode($doctors) : false; ?>;

    $(function(){
        /*Search Doctor*/
        $("#search_doctor").on("submit",function(){
            var search_val = $("#search_txt").val();
            var input = $("<input>").attr({"type"  : "hidden" , "name" : "search"}).val(search_val);
            $(this).append($(input));
            return true;
        });

        /*Keyboard Functionalities*/
        $(".action-button").on("click",function(){
            var container = $("#search_txt");
            var current_value = container.val();
            switch(($(this).text()).toLowerCase()){
                case "space" : 
                    current_value += " ";
                break;
                case "search" : 
                    current_value += "";
                break;
                case "back" : 
                    current_value += "";
                break;
                case "help" : 
                    current_value += "";
                break;
                case "":
                    var icon = $(this).children('i');
                    if(icon.hasClass("fa-arrow-left")){
                        var len = current_value.length;
                        current_value  = current_value.substr(0,len-1);
                    }
                break;
                default : 
                current_value += $(this).text();
            }
            container.val(current_value);
            $('#search_txt').trigger('change'); 
        });
    });

</script>
<?php include('footer.php');?>