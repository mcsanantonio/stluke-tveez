<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Emergency Services</h1>
  <div class="bg_area">
  <div class="main-content-area">
  <h2>Emergency Services</h2>
  <p>The St. Luke's Emergency Department is composed of a group of adult emergency physicians trained in Emergency Medicine and pediatric specialists with a background in Pediatric Emergency. It can handle medical, surgical and toxicologic emergencies in adults and children 24 hours a day. The latest in diagnostic modalities and therapeutics can be made available to patients on a timely basis.</p>
  <h3 class="space">Classification of Emergency Patients</h3>
  <ul class="upper-alpha">
    <li>Primary - patient with non-emergent problems that do not pose life threats now or in the future. Little treatment is necessary.</li>
    <li>Acute - patient needs immediate medical attention because of urgent but not life-threatening problems.</li>
    <li>Critical - patient needs immediate evaluation and/or treatment due to the life-threatening nature of his condition.</li>
  </ul>
  <ol class="decimal">
    <li>Trauma</li>
    <li>Non-trauma</li>
  </ol>
  <ul class="lower-alpha">
    <li>Medical</li>
    <li>Chest pain area</li>
    <li>Isolation Room - a truly negative pressure area where patients with airborne diseases (Tuberculosis, Chicken Pox, etc.) are seen before they are admitted to respective isolation rooms in the hospital.</li>
  </ul>

</div>
</section>
<?php include('footer.php');?>