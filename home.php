<!doctype html>
<html class="no-js" lang="en"oncontextmenu='return false;'>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>St. Luke's</title>
  <style>
    @import url('assets/css/swiper.min.css');
    @import url('assets/css/font-awesome.min.css');
  </style>
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="css/vp2_html5_bottomPlaylist.css" >
  
  <script src="assets/js/jquery-1.11.2.min.js"></script>
  <script src="jquery-ui/jquery-ui.min.js"></script>
  
  <script src="js/reflection.js"></script>
  <script src="js/vp2_html5_bottomPlaylist.js"></script>
  <script src="js/screenfull.min.js"></script>
  
</head>
<body id="the_frame">
<header>
  <a id="logo" href="home.php">St. Luke's</a>
</header>
<?php include('nav.php');?>
<section>
      <div class="vp2_html5_bottomPlaylistBorder">
          <div class="vp2_html5_bottomPlaylist">
            <video class="UAvideo" id="vp2_html5_bottomPlaylist_UB" height="650" width="1570" preload="auto" autoplay>
              <div class="xplaylist">
                <ul>
                  <li class="xsources_mp4">videos/buld.mp4</li>
                  <li class="xsources_webm">videos/buld.webm</li>
                </ul>
                <ul>
                  <li class="xsources_mp4">videos/sl01.mp4</li>
                  <li class="xsources_webm">videos/sl01.webm</li>
                </ul>
                <ul>
                  <li class="xsources_mp4">videos/sl02.mp4</li>
                  <li class="xsources_webm">videos/sl02.webm</li>
                </ul>
                <ul>
                  <li class="xsources_mp4">videos/sl03.mp4</li>
                  <li class="xsources_webm">videos/sl03.webm</li>
                </ul>
                <ul>
                  <li class="xsources_mp4">videos/sl04.mp4</li>
                  <li class="xsources_webm">videos/sl04.webm</li>
                </ul>
                <ul>
                  <li class="xsources_webm">videos/sl05.webm</li>
                </ul>
                <ul>
                  <li class="xsources_webm">videos/sl06.webm</li>
                </ul>
              </div>
            </video>
          </div>
        </div>
        <br style="clear:both;">
      </div>
</section>
<script>
$(function() {
  jQuery('#vp2_html5_bottomPlaylist_UB').vp2_html5_bottomPlaylist_Video({
    responsive:true,
    responsiveRelativeToBrowser:true,
    showInfo: false,
    autoPlay: true,
    loop: true,
	borderWidth:0,
	initialVolume:1,
	numberOfThumbsPerScreen:0,
  });
});
</script>
<?php include('footer.php');?>