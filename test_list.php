<?php
  include('header.php');
  include('functions/search.php');

  $search = isset($_GET['search'])? addslashes($_GET['search']) : "";
  $search = trim($_GET['search']);

  $result = search("test_procedure","test_proc_name",$search);
?>

<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Tests &amp; Procedures lists</h1>
  <div id="records">Number of records found : <span><?php echo $result->num_rows?></span></div>
  <ul class="doclist" id="doclist_title">
	<li>&nbsp;</li>
	<li>PROCEDURE</li>
  </ul>
  <div class="bg_area" id="all_list">
<?php if($result->num_rows > 0) :?>
  <?php $counter = 1;?>
    <?php while($proc = $result->fetch_object()):?>
	<a onclick="clicksound.playclip()" href="map-main.php?id=<?php echo $proc->map_id; ?>" style="padding: 10px; <?php echo $counter++%2 == 1 ? 'background: #FFFFFF;' : 'background: #E3F2FD;' ?>">
	<ul class="doclist">
		<li class="view">View Map</li>
		<li><?php echo "{$proc->test_proc_name}" ?></li>
		
	</ul>
	</a>
      <?php endwhile;?>
      <?php else: ?>
      No record found.
      <?php endif;?>
  </div>
</section>
<?php include('footer.php');?>