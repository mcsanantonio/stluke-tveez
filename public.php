<?php include('header.php');?>
<span id="back"><a onclick="clicksound.playclip()" href="javascript:history.back()">Back</a></span>
<section>
  <h1>Public Facilities</h1>
  <div class="the_content"><div class="bsDemo">
	<div class="the_inner floorGF" id="the_leg">
        <span class="floor_name">Ground Floor</span>
	<h2 class="legend">Legend <span style="color:#E70D2F">Click to locate Area</span></h2>
	<div class="symbols_legent">
	
<input type="radio" id="radio1" name="facilities" value="all" class="5">
   <label for="radio1">Auditorium (Henry Sy, Sr.)</label>
   
<input type="radio" id="radio2" name="facilities" value="false">
   <label for="radio2">Banks/ATM</label>
   
<input type="radio" id="radio3" name="facilities" value="false" class="5">
   <label for="radio3">Cafeteria</label>
   
<input type="radio" id="radio4" name="facilities" value="false" class="5">
   <label for="radio4">Central Sterile Supply Dispensing</label>
   
<input type="radio" id="radio5" name="facilities" value="false" class="5">
   <label for="radio5">Chapel</label>
   
<input type="radio" id="radio6" name="facilities" value="false" class="5">
   <label for="radio6">Conference Rooms</label>
   
<input type="radio" id="radio7" name="facilities" value="false">
   <label for="radio7">Entrances/Exits</label>
   
<input type="radio" id="radio8" name="facilities" value="false" class="5">
   <label for="radio8">Elevator</label>
   
<input type="radio" id="radio9" name="facilities" value="false">
   <label for="radio9">Escalator</label>
   
<input type="radio" id="radio10" name="facilities" value="false">
   <label for="radio10">MedExpress</label>
   
<input type="radio" id="radio11" name="facilities" value="false" class="5">
   <label for="radio11">Pharmacy</label>
   
<input type="radio" id="radio12" name="facilities" value="false">
   <label for="radio12">Restaurants/Shop</label>
   
<input type="radio" id="radio13" name="facilities" value="false">
   <label for="radio13">Restroom</label>
   
<input type="radio" id="radio14" name="facilities" value="false">
   <label for="radio14">Stairs</label>
   
</div>
   <style>
input[type=radio] {
    display:none;
}
 
input[type=radio] + label {
    display:inline-block;
    width:350px;
    margin:-2px;
    padding: 15px 25px;
    margin-bottom: 0;
    font-size: 18px;
    line-height: 20px;
    color: #333;
    text-align: left;
    text-shadow: 0 1px 1px rgba(255,255,255,0.75);
    vertical-align: middle;
    cursor: pointer;
    background-color: #f5f5f5;
    background-image: -moz-linear-gradient(top,#fff,#e6e6e6);
    background-image: -webkit-gradient(linear,0 0,0 100%,from(#fff),to(#e6e6e6));
    background-image: -webkit-linear-gradient(top,#fff,#e6e6e6);
    background-image: -o-linear-gradient(top,#fff,#e6e6e6);
    background-image: linear-gradient(to bottom,#fff,#e6e6e6);
    background-repeat: repeat-x;
    border: 1px solid #ccc;
    border-color: #e6e6e6 #e6e6e6 #bfbfbf;
    border-color: rgba(0,0,0,0.1) rgba(0,0,0,0.1) rgba(0,0,0,0.25);
    border-bottom-color: #b3b3b3;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff',endColorstr='#ffe6e6e6',GradientType=0);
    filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);
    -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);
    box-shadow: inset 0 1px 0 rgba(255,255,255,0.2),0 1px 2px rgba(0,0,0,0.05);
}
 
input[type=radio]:checked + label {
    background-image: none;
    outline: 0;
    -webkit-box-shadow: inset 0 2px 4px rgba(0,0,0,0.15),0 1px 2px rgba(0,0,0,0.05);
    -moz-box-shadow: inset 0 2px 4px rgba(0,0,0,0.15),0 1px 2px rgba(0,0,0,0.05);
    box-shadow: inset 0 2px 4px rgba(0,0,0,0.15),0 1px 2px rgba(0,0,0,0.05);
    background-color:#e0e0e0;
}
   </style>
	<span style="display:block;padding:40px 30px 0;font-size:22px">*Parking is in basement 1 to 4</span>
	</div>
    </div>
    
    </div>
</section>
<?php include('footer.php');?>
